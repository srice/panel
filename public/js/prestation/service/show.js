/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/prestation/service/show.js":
/*!*************************************************!*\
  !*** ./resources/js/prestation/service/show.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var tableListeTarifs;
var spinnerClass = 'spinner spinner-right spinner-success pr-15';
var spinnerMessage = 'Veuillez patienter';

function queryListeTable() {
  tableListeTarifs = $("#listeTarifs").KTDatatable({
    data: {
      saveState: {
        cookie: false
      }
    },
    search: {
      input: $('#search_table_tarifs'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'id',
      type: 'number',
      width: 10,
      autoHide: true
    }, {
      field: 'name',
      width: 50
    }, {
      field: 'amount',
      width: 50
    }, {
      field: 'Action',
      autoHide: false
    }],
    translate: {
      records: {
        processing: 'Chargement...',
        noRecords: 'Aucun élément correspondant trouvé'
      },
      toolbar: {
        pagination: {
          items: {
            "default": {
              first: 'Premier',
              prev: 'Précédent',
              next: 'Suivant',
              last: 'Dernier',
              more: 'Voir plus',
              input: 'Numéro de Page',
              select: 'Nombre d\'affichage par page'
            },
            info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments'
          }
        }
      }
    }
  });
}

function formSubmitAddTarif() {
  $("#formAddTarif").on('submit', function (e) {
    e.preventDefault();
    var form = $(this);
    var btn = KTUtil.getById('btnSubmitForm');
    var url = form.attr('action');
    var data = form.serializeArray();
    KTUtil.btnWait(btn, spinnerClass, spinnerMessage);
    $.ajax({
      url: url,
      method: 'POST',
      data: data,
      statusCode: {
        201: function _() {
          KTUtil.btnRelease(btn);
          toastr.success("Le tarif à été ajouté avec succès");
          $("#addTarif").modal('hide');
          tableListeTarifs.reload();
        },
        422: function _(data) {
          KTUtil.btnRelease(btn);
          toastr.warning(data.data);
        },
        500: function _(jqxhr) {
          KTUtil.btnRelease(btn);
          toastr.error("Erreur 500: Erreur de script");
          console.error(jqxhr.responseText);
        }
      }
    });
  });
}

jQuery(document).ready(function () {
  $.noConflict();
  queryListeTable();
  formSubmitAddTarif();
});

/***/ }),

/***/ 10:
/*!*******************************************************!*\
  !*** multi ./resources/js/prestation/service/show.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\LOGICIEL\laragon\www\gestion.srice\resources\js\prestation\service\show.js */"./resources/js/prestation/service/show.js");


/***/ })

/******/ });