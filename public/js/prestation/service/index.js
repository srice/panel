/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/prestation/service/index.js":
/*!**************************************************!*\
  !*** ./resources/js/prestation/service/index.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var tableFamille;
var tableService;
var divModule = document.getElementById('divModule');
divModule.style.display = 'none';
var spinnerClass = 'spinner spinner-right spinner-success pr-15';
var spinnerMessage = 'Veuillez patienter';

function listeFamille() {
  tableFamille = $("#listeFamille").KTDatatable({
    data: {
      saveState: {
        cookie: false
      }
    },
    search: {
      input: $('#kt_datatable_search_query_famille'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'id',
      type: 'number',
      width: 50
    }, {
      field: 'Action',
      autoHide: false
    }],
    translate: {
      records: {
        processing: 'Chargement...',
        noRecords: 'Aucun élément correspondant trouvé'
      },
      toolbar: {
        pagination: {
          items: {
            "default": {
              first: 'Premier',
              prev: 'Précédent',
              next: 'Suivant',
              last: 'Dernier',
              more: 'Voir plus',
              input: 'Numéro de Page',
              select: 'Nombre d\'affichage par page'
            },
            info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments'
          }
        }
      }
    }
  });
}

function listeService() {
  tableService = $("#listeServices").KTDatatable({
    data: {
      saveState: {
        cookie: false
      }
    },
    search: {
      input: $('#kt_datatable_search_query'),
      key: 'generalSearch'
    },
    columns: [{
      field: 'id',
      type: 'number',
      width: 50
    }, {
      field: 'Action',
      autoHide: false
    }],
    translate: {
      records: {
        processing: 'Chargement...',
        noRecords: 'Aucun élément correspondant trouvé'
      },
      toolbar: {
        pagination: {
          items: {
            "default": {
              first: 'Premier',
              prev: 'Précédent',
              next: 'Suivant',
              last: 'Dernier',
              more: 'Voir plus',
              input: 'Numéro de Page',
              select: 'Nombre d\'affichage par page'
            },
            info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments'
          }
        }
      }
    }
  });
}

function validateFormFamille() {
  var vff = FormValidation.formValidation(document.getElementById('formAddFamille'), {
    fields: {
      name: {
        validators: {
          notEmpty: {
            message: "Le nom de la famille de service est requis"
          }
        }
      }
    },
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      //tachyons: new FormValidation.plugins.Tachyons(),
      submitButton: new FormValidation.plugins.SubmitButton(),
      bootstrap: new FormValidation.plugins.Bootstrap(),
      icon: new FormValidation.plugins.Icon({
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
      })
    }
  }).on('core.form.valid', function (e) {
    var form = $("#formAddFamille");
    var btn = KTUtil.getById('btnSubmitFormFamille');
    var url = form.attr('action');
    var data = form.serializeArray();
    KTUtil.btnWait(btn, spinnerClass, spinnerMessage);
    $.ajax({
      url: url,
      method: "POST",
      data: data,
      statusCode: {
        201: function _(data) {
          KTUtil.btnRelease(btn);
          tableFamille.reload();
          toastr.success("La famille de service <strong>".concat(data.name, "</strong> \xE0 \xE9t\xE9 cr\xE9er avec succ\xE8s"), "Succès");
        },
        422: function _(data) {
          KTUtil.btnRelease(btn);
          Array.from(data.data.errors).forEach(function (err) {
            toastr.warning(err, "Validation");
          });
        },
        500: function _(jqxhr) {
          KTUtil.btnRelease(btn);
          toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
          console.log(jqxhr.responseText);
        }
      }
    });
  });
}

function validateFormService() {
  var vff = FormValidation.formValidation(document.getElementById('formAddService'), {
    fields: {
      famille_id: {
        validators: {
          notEmpty: {
            message: "La famille est requise"
          }
        }
      },
      name: {
        validators: {
          notEmpty: {
            message: "Le nom du service est requis"
          }
        }
      },
      kernel: {
        validators: {
          notEmpty: {
            message: "Le type de service est requis"
          }
        }
      },
      name_tarif: {
        validators: {
          notEmpty: {
            message: "Le nom du tarif de service est requis"
          }
        }
      },
      montant_tarif: {
        validators: {
          notEmpty: {
            message: "Le montant du tarif de service est requis"
          }
        }
      }
    },
    plugins: {
      trigger: new FormValidation.plugins.Trigger(),
      //tachyons: new FormValidation.plugins.Tachyons(),
      submitButton: new FormValidation.plugins.SubmitButton(),
      bootstrap: new FormValidation.plugins.Bootstrap(),
      icon: new FormValidation.plugins.Icon({
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
      })
    }
  }).on('core.form.valid', function (e) {
    var form = $("#formAddService");
    var btn = KTUtil.getById('btnSubmitFormService');
    var url = form.attr('action');
    var data = form.serializeArray();
    KTUtil.btnWait(btn, spinnerClass, spinnerMessage);
    $.ajax({
      url: url,
      method: "POST",
      data: data,
      statusCode: {
        201: function _(data) {
          KTUtil.btnRelease(btn);
          tableService.reload();
          toastr.success("Le service <strong>".concat(data.name, "</strong> \xE0 \xE9t\xE9 cr\xE9er avec succ\xE8s"), "Succès");
        },
        422: function _(data) {
          KTUtil.btnRelease(btn);
          Array.from(data.data.errors).forEach(function (err) {
            toastr.warning(err, "Validation");
          });
        },
        500: function _(jqxhr) {
          KTUtil.btnRelease(btn);
          toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
          console.log(jqxhr.responseText);
        }
      }
    });
  });
}

function deleteFamille() {
  tableFamille.on('click', '#btnDeleteFamille', function (e) {
    e.preventDefault();
    var btn = $(this);
    var url = btn.attr('href');
    KTUtil.btnWait(btn, spinnerClass, spinnerMessage);
    $.ajax({
      url: url,
      method: "GET",
      statusCode: {
        201: function _(data) {
          KTUtil.btnRelease(btn);
          tableFamille.reload();
          toastr.success("La famille <strong>".concat(data.name, "</strong> \xE0 \xE9t\xE9 supprimer avec succ\xE8s"), "Succès");
        },
        203: function _(data) {
          KTUtil.btnRelease(btn);
          toastr.warning("La famille <strong>".concat(data.name, "</strong> n'\xE0 pas \xE9t\xE9 supprimer car des services lui sont affili\xE9"), "Attention");
        },
        500: function _(jqxhr) {
          KTUtil.btnRelease(btn);
          toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
          console.log(jqxhr.responseText);
        }
      }
    });
  });
}

function deleteService() {
  var _this = this;

  tableService.on('click', 'btnDeleteService', function (e) {
    e.preventDefault();
    var btn = KTUtil.getById($(_this));
    var url = $(_this).attr('href');
    KTUtil.btnWait(btn, spinnerClass);
    $.ajax({
      url: url,
      method: 'GET',
      success: function success(data) {
        KTUtil.btnRelease(btn);
      },
      error: function error(jqxhr) {
        KTUtil.btnRelease(btn);
      }
    });
  });
}

jQuery(document).ready(function () {
  $.noConflict();
  listeService();
  listeFamille();
  validateFormFamille();
  deleteFamille();
  validateFormService();
  deleteService();
  $("#selectKernel").on('change', function () {
    var value = $(this).val();

    if (value == 2) {
      divModule.style.display = 'block';
    } else {
      divModule.style.display = 'none';
    }
  });
});

/***/ }),

/***/ 9:
/*!********************************************************!*\
  !*** multi ./resources/js/prestation/service/index.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\LOGICIEL\laragon\www\gestion.srice\resources\js\prestation\service\index.js */"./resources/js/prestation/service/index.js");


/***/ })

/******/ });