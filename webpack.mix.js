const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/auth/login.js', 'public/js/auth')
    .js('resources/js/auth/register.js', 'public/js/auth')

    .js('resources/js/comite/index.js', 'public/js/comite')
    .js('resources/js/comite/create.js', 'public/js/comite')
    .js('resources/js/comite/show.js', 'public/js/comite')
    .js('resources/js/comite/contact/create.js', 'public/js/comite/contact')
    .js('resources/js/comite/contact/edit.js', 'public/js/comite/contact')
    .js('resources/js/comite/paiment/create.js', 'public/js/comite/paiment')

    .js('resources/js/prestation/service/index.js', 'public/js/prestation/service')
    .js('resources/js/prestation/service/show.js', 'public/js/prestation/service')
    .js('resources/js/prestation/service/edit.js', 'public/js/prestation/service')
    .js('resources/js/prestation/module/index.js', 'public/js/prestation/module')
    .js('resources/js/prestation/module/show.js', 'public/js/prestation/module')
    .sass('resources/sass/app.scss', 'public/css');
