<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('comite_id')->unsigned();
            $table->timestamp('date');
            $table->string('amount')->default(0);
            $table->integer('state')->default(0)->comment("0: Brouillon |1: Valider |2: En cours de traitement |3:Finaliser |4: Annuler");
            $table->integer('espaceCheck')->nullable()->default(0)->comment("Dépend ou non d'un espace");
            $table->bigInteger('espace_id')->nullable()->unsigned();

            $table->foreign('comite_id')->references('id')->on('comites')->onDelete('cascade');
            //$table->foreign('espace_id')->references('id')->on('espaces')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
