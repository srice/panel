<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravauxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travauxes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('travaux_secteur_id')->nullable()->unsigned();
            $table->integer('type')->default(\App\Model\Support\Travaux\Travaux::TASK_TYPE_INCIDENT);
            $table->string('summary');
            $table->text('details');
            $table->integer('status')->default(\App\Model\Support\Travaux\Travaux::TASK_STATUS_PROGRESS);
            $table->string('progress')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('travaux_secteur_id')->references('id')->on('travaux_secteurs')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travauxes');
    }
}
