<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspaceServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espace_services', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('espace_id')->nullable()->unsigned();
            $table->bigInteger('server_id')->nullable()->unsigned();
            $table->string('database');

            $table->foreign('espace_id')->references('id')->on('espaces')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espace_services');
    }
}
