<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espaces', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('comite_id')->nullable()->unsigned();
            $table->bigInteger('contrat_id')->nullable()->unsigned();
            $table->string('domain');
            $table->string('path');
            $table->integer('state')->default(0);
            $table->string('install')->default(0);
            $table->string('errorLog')->nullable();

            $table->foreign('comite_id')->references('id')->on('comites')->cascadeOnDelete();
            $table->foreign('contrat_id')->references('id')->on('contrats')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espaces');
    }
}
