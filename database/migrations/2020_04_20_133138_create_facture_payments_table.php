<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('facture_id')->unsigned();
            $table->bigInteger('payment_id')->unsigned();
            $table->timestamp('date');
            $table->string('amount');
            $table->integer('state')->default(0)->comment("0: En cours d'execution |1: Executer |2: Impossible d'executer le paiement");

            $table->foreign('facture_id')->references('id')->on('factures')->cascadeOnDelete();
            $table->foreign('payment_id')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_payments');
    }
}
