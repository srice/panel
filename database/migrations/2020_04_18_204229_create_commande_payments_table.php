<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commande_payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('commande_id')->unsigned();
            $table->bigInteger('payment_id')->unsigned();
            $table->timestamp('date');
            $table->string('amount');
            $table->integer('state')->default(0)->comment("0: En cours d'execution |1: Executer |2: Impossible d'executer le paiement");

            $table->foreign('commande_id')->references('id')->on('commandes')->cascadeOnDelete();
            $table->foreign('payment_id')->references('id')->on('payments')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commande_payments');
    }
}
