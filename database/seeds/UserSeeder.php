<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('fr_FR');
        $password = \Illuminate\Support\Facades\Hash::make('0000');

        \App\User::create([
            "name" => "administrator",
            "email" => "admin@test.com",
            "password" => $password,
            "api_token" => "TBdZ8Q2aNbYBceYYUdxAWGXvnfJMIKqkMo94lTDJniSBHfV9zLUGSaBP2a4y"
        ]);

        for ($i = 0; $i < 5; $i++) {
            \App\User::create([
                "name" => $faker->name,
                "email" => $faker->email,
                "password" => $password
            ]);
        }
    }
}
