<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArticleSeeder::class);
        $this->call(UserSeeder::class);
        if(env('APP_ENV') == 'local') {
            $comite = factory(\App\Model\Comite\Comite::class)->create();
            $user = factory(\App\User::class)->create();
            factory(\App\Model\Comite\Contact::class)->create([
                "comite_id" => $comite->id,
                "user_id" => $user->id
            ]);
            $payment = factory(\App\Model\Comite\Payment::class)->create([
                "comite_id" => $comite->id,
                "stripe_id" => "card_1GhxmiAyWfYU9MALHVA1WUXd"
            ]);

            // TODO: Prestation

            factory(\App\Model\Prestation\Famille::class)->create();
            $service = factory(\App\Model\Prestation\Service::class)->create();
            $tarif = factory(\App\Model\Prestation\ServiceTarif::class)->create([
                "service_id" => $service->id
            ]);

            $module = factory(\App\Model\Prestation\Module::class)->create();
            factory(\App\Model\Prestation\ModuleChangelog::class)->create([
                "module_id" => $module->id
            ]);
            factory(\App\Model\Prestation\ModuleTask::class)->create([
                "module_id" => $module->id
            ]);


            // TODO: Facturation
            // // TODO: Commande
            $commande = factory(\App\Model\Facturation\Commande\Commande::class)->create([
                "comite_id" => $comite->id
            ]);
            factory(\App\Model\Facturation\Commande\CommandeItem::class)->create([
                "commande_id" => $commande->id,
                "service_id" => $service->id,
                "service_tarif_id" => $tarif->id
            ]);
            factory(\App\Model\Facturation\Commande\CommandePayment::class)->create([
                "commande_id" => $commande->id,
                "payment_id" => $payment->id
            ]);

            // // TODO: Facture
            $facture = factory(\App\Model\Facturation\Facture::class)->create([
                "commande_id" => $commande->id,
                "comite_id" => $comite->id
            ]);
            $item = factory(\App\Model\Facturation\FactureItem::class)->create([
                "facture_id" => $facture->id,
                "service_id" => $service->id,
            ]);
            $payment = factory(\App\Model\Facturation\FacturePayment::class)->create([
                "facture_id" => $facture->id,
                "payment_id" => $payment->id
            ]);

            // // TODO: Contrat
            $c_famille = factory(\App\Model\Facturation\Contrat\ContratFamille::class)->create([
                "service_id" => $service->id
            ]);
            $contrat = factory(\App\Model\Facturation\Contrat\Contrat::class)->create([
                "comite_id" => $comite->id,
                "contrat_famille_id" => $c_famille->id,
                "commande_id" => $commande->id
            ]);


            // TODO: Espace
            $espace = factory(\App\Model\Espace\Espace::class)->create([
                "comite_id" => $comite->id,
                "contrat_id" => $contrat->id
            ]);
            $install = factory(\App\Model\Espace\EspaceInstall::class)->create(["espace_id" => $espace->id]);
            $licence = factory(\App\Model\Espace\EspaceLicence::class)->create(["espace_id" => $espace->id]);
            $e_module = factory(\App\Model\Espace\EspaceModule::class)->create(["espace_id" => $espace->id, "module_id" => $module->id]);


            // TODO: Infrastructure

            $registar = factory(\App\Model\Infrastructure\Server\ServerRegistar::class)->create();
            $server = factory(\App\Model\Infrastructure\Server\Server::class)->create(["registar_id" => $registar->id]);

            $e_service = factory(\App\Model\Espace\EspaceService::class)->create(["espace_id" => $espace->id]);

            // TODO: Webservice
            // // TODO: Blog
            $b_category = factory(\App\Model\Webservice\BlogCategory::class)->create();
            $blog = factory(\App\Model\Webservice\Blog::class)->create([
                "blog_category_id" => $b_category->id
            ]);

            // TODO: Support
            // // TODO: Ticket
            $t_category = factory(\App\Model\Support\Ticket\TicketCategory::class)->create();
            $ticket = factory(\App\Model\Support\Ticket\Ticket::class)->create(["ticket_category_id" => $t_category->id, "comite_id" => $comite->id]);
            $conv = factory(\App\Model\Support\Ticket\TicketConversation::class)->create(["ticket_id" => $ticket->id]);

            // // TODO: Travaux
            $t_secteur = factory(\App\Model\Support\Travaux\TravauxSecteur::class)->create();
            $travaux = factory(\App\Model\Support\Travaux\Travaux::class)->create(["travaux_secteur_id" => $t_secteur->id]);
            $comment = factory(\App\Model\Support\Travaux\TravauxComment::class)->create(["travaux_id" => $travaux->id]);

        }
    }
}
