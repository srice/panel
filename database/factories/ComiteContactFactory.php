<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Comite\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {
    return [
        "comite_id" => null,
        "user_id" => null,
        "position" => "Position de Test",
        "telephone" => "00 00 00 00 01"
    ];
});
