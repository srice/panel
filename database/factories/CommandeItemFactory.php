<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Facturation\Commande\CommandeItem::class, function (Faker $faker) {
    return [
        "commande_id" => null,
        "service_id" => null,
        "service_tarif_id" => null,
        "description" => $faker->text,
        "quantite" => 1,
        "amount" => 12.00
    ];
});
