<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Prestation\ModuleChangelog::class, function (Faker $faker) {
    return [
        "module_id" => null,
        "version" => $faker->numberBetween(0,9).'.'.$faker->numberBetween(0,9).'.'.$faker->numberBetween(0,9),
        "description" => $faker->text,
        "date" => now()->subDays(rand(0,365)),
        "state" => 2
    ];
});
