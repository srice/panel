<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Webservice\Blog::class, function (Faker $faker) {
    return [
        "blog_category_id" => null,
        "title" => $faker->sentence,
        "slug" => $faker->slug,
        "content" => $faker->realText(),
    ];
});
