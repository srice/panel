<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Support\Travaux\Travaux::class, function (Faker $faker) {
    return [
        "travaux_secteur_id" => null,
        "type" => Model\Support\Travaux\Travaux::TASK_TYPE_INCIDENT,
        "summary" => "Incident de Blocage Totp",
        "details" => "Une erreur d'authentification retourne une erreur dans toute les situations par appel API Oauth de SRICE.",
        "status" => Model\Support\Travaux\Travaux::TASK_STATUS_PROGRESS,
        "progress" => 0
    ];
});
