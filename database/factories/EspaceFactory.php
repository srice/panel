<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Espace\Espace::class, function (Faker $faker) {
    return [
        "comite_id" => null,
        "contrat_id" => null,
        "domain" => "test",
        "path" => "test",
        "state" => 0,
        "install" => 0
    ];
});
