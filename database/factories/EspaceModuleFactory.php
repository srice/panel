<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Espace\EspaceModule::class, function (Faker $faker) {
    return [
        "espace_id" => null,
        "module_id" => null,
        "state" => 0,
        "checkout" => 0
    ];
});
