<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(Model\Infrastructure\Server\ServerRegistar::class, function (Faker $faker) {
    return [
        "name" => "OVH"
    ];
});
