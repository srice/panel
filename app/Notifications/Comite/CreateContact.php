<?php

namespace App\Notifications\Comite;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CreateContact extends Notification
{
    use Queueable;
    private $contact;
    private $password;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @param $contact
     * @param $user
     * @param $password
     */
    public function __construct($contact, $user, $password)
    {
        //
        $this->contact = $contact;
        $this->password = $password;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Bienvenue sur le logiciel SRICE")
            ->greeting('Bonjour '.$this->user->name)
            ->line('Votre compte à bien été créer')
            ->line('Identifiant: '.$this->user->email)
            ->line('Mot de Passe: '.$this->password)
            ->action('Panel Cloud SRICE', env('APP_URL'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
