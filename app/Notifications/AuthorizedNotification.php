<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AuthorizedNotification extends Notification
{
    use Queueable;
    private $secteur;
    private $status;
    private $text;
    private $icon;

    /**
     * Create a new notification instance.
     *
     * @param $secteur
     * @param $status
     * @param $text
     * @param $icon
     */
    public function __construct($secteur, $status, $text, $icon)
    {
        //
        $this->secteur = $secteur;
        $this->status = $status;
        $this->text = $text;
        $this->icon = $icon;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "secteur" => $this->secteur,
            "status" => $this->status,
            "text" => $this->text,
            "icon" => $this->icon
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            "secteur" => $this->secteur,
            "status" => $this->status,
            "text" => $this->text,
            "icon" => $this->icon
        ]);
    }
}
