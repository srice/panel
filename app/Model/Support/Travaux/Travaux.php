<?php

namespace App\Model\Support\Travaux;

use Illuminate\Database\Eloquent\Model;

class Travaux extends Model
{
    protected $guarded = [];

    const TASK_TYPE_MAINTENANCE = 1;
    const TASK_TYPE_INCIDENT = 2;
    const TASK_TYPE_IMPROVEMENT = 3;
    const TASK_TYPE_UPDATE = 4;

    const TASK_STATUS_PROGRESS = 1;
    const TASK_STATUS_CLOSE = 2;
    const TASK_STATUS_PLANNED = 3;

    public function secteur()
    {
        return $this->belongsTo(TravauxSecteur::class, 'travaux_secteur_id');
    }

    public function comments()
    {
        return $this->hasMany(TravauxComment::class);
    }
}
