<?php

namespace App\Model\Support\Travaux;

use Illuminate\Database\Eloquent\Model;

class TravauxComment extends Model
{
    protected $guarded = [];

    public function travaux()
    {
        return $this->belongsTo(Travaux::class, 'travaux_id');
    }
}
