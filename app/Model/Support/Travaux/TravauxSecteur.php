<?php

namespace App\Model\Support\Travaux;

use Illuminate\Database\Eloquent\Model;

class TravauxSecteur extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function travauxes()
    {
        return $this->hasMany(Travaux::class);
    }
}
