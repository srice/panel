<?php

namespace App\Model\Support\Ticket;

use App\Model\Comite\Comite;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];
    const STATUS_NEW = 1;
    const STATUS_OPEN = 2;
    const STATUS_PENDING = 3;
    const STATUS_SOLVED = 4;
    const STATUS_CLOSED = 5;

    const PRIORITY_LOW       = 1;
    const PRIORITY_NORMAL    = 2;
    const PRIORITY_HIGH      = 3;
    const PRIORITY_BLOCKER   = 4;

    public function ticket_category()
    {
        return $this->belongsTo(TicketCategory::class, 'ticket_category_id');
    }

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function conversations()
    {
        return $this->hasMany(TicketConversation::class);
    }
}
