<?php

namespace App\Model\Comite;

use App\Model\Facturation\Commande\CommandePayment;
use App\Model\Facturation\FacturePayment;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    const PAYMENT_STATUS_VERIFIED_NONE = 0;
    const PAYMENT_STATUS_VERIFIED_OK = 1;

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function commande_payments()
    {
        return $this->hasMany(CommandePayment::class);
    }

    public function facture_payments()
    {
        return $this->hasMany(FacturePayment::class);
    }
}
