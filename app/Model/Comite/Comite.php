<?php

namespace App\Model\Comite;

use App\Model\Espace\Espace;
use App\Model\Facturation\Commande\Commande;
use App\Model\Facturation\Contrat\Contrat;
use App\Model\Facturation\Facture;
use App\Model\Support\Ticket\Ticket;
use Illuminate\Database\Eloquent\Model;

class Comite extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    public function factures()
    {
        return $this->hasMany(Facture::class);
    }

    public function contrats()
    {
        return $this->hasMany(Contrat::class);
    }

    public function espaces()
    {
        return $this->hasMany(Espace::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
