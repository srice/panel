<?php

namespace App\Model\Espace;

use App\Model\Infrastructure\Server\Server;
use Illuminate\Database\Eloquent\Model;

class EspaceService extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function espace()
    {
        return $this->belongsTo(Espace::class, 'espace_id');
    }

    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }
}
