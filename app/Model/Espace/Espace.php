<?php

namespace App\Model\Espace;

use App\Model\Comite\Comite;
use App\Model\Facturation\Contrat\Contrat;
use Illuminate\Database\Eloquent\Model;

class Espace extends Model
{
    protected $guarded = [];
    public $timestamps = false;


    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function contrat()
    {
        return $this->belongsTo(Contrat::class, 'contrat_id');
    }

    public function service()
    {
        return $this->hasOne(EspaceService::class);
    }

    public function installs()
    {
        return $this->hasMany(EspaceInstall::class);
    }

    public function licences()
    {
        return $this->hasMany(EspaceLicence::class);
    }

    public function modules()
    {
        return $this->hasMany(EspaceModule::class);
    }
}
