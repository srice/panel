<?php

namespace App\Model\Infrastructure\Server;

use App\Model\Espace\EspaceService;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function registar()
    {
        return $this->belongsTo(ServerRegistar::class, 'registar_id');
    }

    public function services()
    {
        return $this->hasMany(EspaceService::class);
    }
}
