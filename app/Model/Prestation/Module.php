<?php

namespace App\Model\Prestation;

use App\Model\Espace\EspaceModule;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function changelogs()
    {
        return $this->hasMany(ModuleChangelog::class);
    }

    public function tasks()
    {
        return $this->hasMany(ModuleTask::class);
    }

    public function espace_modules()
    {
        return $this->hasMany(EspaceModule::class);
    }
}
