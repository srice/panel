<?php

namespace App\Model\Facturation\Contrat;

use App\Model\Comite\Comite;
use App\Model\Espace\Espace;
use App\Model\Facturation\Commande\Commande;
use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function comite()
    {
        return $this->belongsTo(Comite::class, 'comite_id');
    }

    public function famille()
    {
        return $this->belongsTo(ContratFamille::class, 'contrat_famille_id');
    }

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commande_id');
    }

    public function espace()
    {
        return $this->hasOne(Espace::class);
    }
}
