<?php

namespace App\Model\Facturation\Commande;

use App\Model\Prestation\Service;
use App\Model\Prestation\ServiceTarif;
use Illuminate\Database\Eloquent\Model;

class CommandeItem extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commande_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function tarif()
    {
        return $this->belongsTo(ServiceTarif::class, 'service_tarif_id');
    }
}
