<?php

namespace App\Model\Facturation\Commande;

use App\Model\Comite\Payment;
use Illuminate\Database\Eloquent\Model;

class CommandePayment extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commande_id');
    }

    public function mode_payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }
}
