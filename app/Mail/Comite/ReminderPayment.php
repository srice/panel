<?php

namespace App\Mail\Comite;

use App\Model\Comite\Comite;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderPayment extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Comite
     */
    private $comite;

    /**
     * Create a new message instance.
     *
     * @param Comite $comite
     */
    public function __construct(Comite $comite)
    {
        //
        $this->comite = $comite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SRICE - Message Important')
            ->view("Mail.Comite.reminderPayment", [
            "comite" => $this->comite
        ]);
    }
}
