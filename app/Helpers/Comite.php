<?php


namespace App\Helpers;


use App\Model\Facturation\Facture;
use App\Model\Support\Ticket\Ticket;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;
use Stripe\Stripe;

class Comite
{
    public static function sumCaForComite($comite_id)
    {
        $facture = new Facture();
        $sum = $facture->newQuery()->where('comite_id', $comite_id)->where('state', 4)->sum('amount');

        return number_format($sum, '2', ',', ' ') . " €";
    }

    public static function countTicketForComite($comite_id)
    {
        $ticket = new Ticket();
        return $ticket->newQuery()->where('comite_id', $comite_id)->where('status', 4)->orWhere('status', 5)->count();
    }

    public static function StripeGetPaymentInfo($stripe_id, $field)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        try {
            $pm = PaymentMethod::retrieve($stripe_id);
            if ($field == null) {
                return $pm;
            } else {
                return $pm->$field;
            }
        } catch (ApiErrorException $e) {
            return $e->getMessage();
        }
    }

    public static function StripeGetCardInfo($stripe_id, $field)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        try {
            $pm = PaymentMethod::retrieve($stripe_id);
            if ($field == null) {
                return $pm->card;
            } else {
                return $pm->card->$field;
            }
        } catch (ApiErrorException $e) {
            return $e->getMessage();
        }
    }

    public static function StripeGetSepaDebitInfo($stripe_id, $field)
    {
        Stripe::setApiKey('sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX');
        try {
            $pm = PaymentMethod::retrieve($stripe_id);
            if ($field == null) {
                return $pm->sepa_debit;
            } else {
                return $pm->sepa_debit->$field;
            }
        } catch (ApiErrorException $e) {
            return $e->getMessage();
        }
    }
}
