<?php


namespace App\Helpers;


class Notifications
{
    public static function getColorStatus($status)
    {
        switch ($status) {
            case 'success': return 'text-success';
            case 'warning': return 'text-warning';
            case 'danger': return 'text-danger';
            case 'info': return 'text-info';
            default: return 'text-primary';
        }
    }
}
