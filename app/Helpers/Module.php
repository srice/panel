<?php


namespace App\Helpers;


class Module
{
    public static function labelStateTask($state)
    {
        switch ($state) {
            case 0: return '<span class="label label-light-dark label-pill label-inline mr-2">Nouveau</span>'; break;
            case 1: return '<span class="label label-warning label-pill label-inline mr-2">En cours</span>'; break;
            case 2: return '<span class="label label-success label-pill label-inline mr-2">Terminer</span>'; break;
            case 3: return '<span class="label label-info label-pill label-inline mr-2">En attente</span>'; break;
            case 4: return '<span class="label label-danger label-pill label-inline mr-2">Annuler</span>'; break;
            default: return null;
        }
    }

    public static function cardRelease($release)
    {
        switch ($release) {
            case 0: return '<div class="card card-custom bgi-no-repeat gutter-b bg-diagonal bg-diagonal-primary bg-diagonal-r-light rounded h-150px" style="height: auto;">
                <!--begin::Body-->
                <div class="card-body d-flex align-items-center">
                    <div class="py-2">
                        <div class="symbol symbol-100 symbol-danger mr-3">
                            <span class="symbol-label font-size-h2">ALPHA</span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>'; break;

            case 1: return '<div class="card card-custom bgi-no-repeat gutter-b bg-diagonal bg-diagonal-light bg-diagonal-r-warning rounded h-150px" style="height: auto;">
                <!--begin::Body-->
                <div class="card-body d-flex align-items-center">
                    <div class="py-2">
                        <div class="symbol symbol-100 symbol-warning mr-3">
                            <span class="symbol-label font-size-h2">BETA</span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>'; break;

            case 2: return '<div class="card card-custom bgi-no-repeat gutter-b bg-diagonal bg-diagonal-light bg-diagonal-r-info rounded h-150px" style="height: auto;">
                <!--begin::Body-->
                <div class="card-body d-flex align-items-center">
                    <div class="py-2">
                        <div class="symbol symbol-100 symbol-info mr-3">
                            <span class="symbol-label font-size-h2">GAMMA</span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>'; break;

            case 3: return '<div class="card card-custom bgi-no-repeat gutter-b bg-diagonal bg-diagonal-light bg-diagonal-r-success rounded h-150px" style="height: auto;">
                <!--begin::Body-->
                <div class="card-body d-flex align-items-center">
                    <div class="py-2">
                        <div class="symbol symbol-100 symbol-success mr-3">
                            <span class="symbol-label font-size-h2">LIVE</span>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>'; break;
        }
    }
}
