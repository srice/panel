<?php


namespace App\Helpers;


class Service
{
    public static function stringTypeService($type)
    {
        switch ($type) {
            case 0: return "Aucune particularité"; break;
            case 1: return "Espace"; break;
            case 2: return "Module"; break;
            default: return null;
        }
    }

    public static function labelModuleRelease($release) {
        switch ($release) {
            case 0: return '<div class="symbol symbol-danger  mr-3">
                <span class="symbol-label font-size-h5">ALPHA</span>
            </div>'; break;

            case 1: return '<div class="symbol symbol-warning  mr-3">
                <span class="symbol-label font-size-h5">BETA</span>
            </div>'; break;

            case 2: return '<div class="symbol symbol-info  mr-3">
                <span class="symbol-label font-size-h5">GAMMA</span>
            </div>'; break;

            case 3: return '<div class="symbol symbol-success  mr-3">
                <span class="symbol-label font-size-h5">LIVE</span>
            </div>'; break;
        }
    }

    public static function textModuleRelease($release)
    {
        switch ($release) {
            case 0: return 'ALPHA'; break;

            case 1: return 'BETA'; break;

            case 2: return 'GAMA'; break;

            case 3: return 'LIVE'; break;
        }
    }
}
