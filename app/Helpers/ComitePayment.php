<?php


namespace App\Helpers;


class ComitePayment
{
    public static function modeAsString($type)
    {
        switch ($type) {
            case 'card': return "Carte Bancaire";
            case 'sepa_debit': return "Prélèvement SEPA";
            default: return "Mode de Paiment inconnue";
        }
    }

    public static function cardIconShow($type_card)
    {
        switch ($type_card) {
            case 'visa': return '<i class="la la-cc-visa"></i>';
            case 'mastercard': return '<i class="la la-cc-mastercard"></i>';
            default: return '<i class="la la-credit-card"></i>';
        }
    }

    public static function statusPayment($status)
    {
        switch ($status)
        {
            case 0: return '<span class="label label-danger label-inline mr-2 text-white">Non vérifier</span>';
            case 1: return '<span class="label label-success label-inline mr-2 text-white">Vérifier</span>';
            default: return 'Status Inconnue';
        }
    }
}
