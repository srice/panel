<?php


namespace App\Helpers;


use App\Model\Comite\Payment;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;

class Stripe
{
    /**
     * @var string
     */
    private $key;

    public function __construct()
    {
        $this->key = "sk_test_u3yFZWKq0KokEDtX2gCiKDbW00H89p1acX";
    }

    public function createCustomer($name, $email, $adresse, $codePostal, $ville, $phone)
    {
        \Stripe\Stripe::setApiKey($this->key);
        try {
            return Customer::create([
                "name" => $name,
                "email" => $email,
                "address" => [
                    "line1" => $adresse,
                    "postal_code" => $codePostal,
                    "city" => $ville
                ],
                "phone" => $phone
            ]);
        }catch (ApiErrorException $exception) {
            return $exception->getMessage();
        }
    }

    public function createSetupPayment($method, $customer_id, $cbnumber = null, $exp_month = null, $exp_year = null, $cvv = null, $iban = null, $comite = null)
    {
        \Stripe\Stripe::setApiKey($this->key);
        try {
            switch ($method) {
                case 1:
                    $p =  PaymentMethod::create([
                        "type" => "card",
                        "card" => [
                            "number" => $cbnumber,
                            "exp_month" => $exp_month,
                            "exp_year" => $exp_year,
                            "cvc" => $cvv
                        ]
                    ])->attach([
                        "customer" => $customer_id
                    ]);
                    $payment = Payment::create([
                        "comite_id" => $comite->id,
                        "stripe_id" => $p->id
                    ]);
                    return $p;
                case 2:
                    $p = PaymentMethod::create([
                        "type" => "sepa_debit",
                        "sepa_debit" => [
                            "iban" => $iban
                        ],
                        "billing_details" => [
                            "name" => $comite->name,
                            "email" => $comite->email
                        ]
                    ])->attach([
                        "customer" => $customer_id
                    ]);
                    $payment = Payment::create([
                        "comite_id" => $comite->id,
                        "stripe_id" => $p->id
                    ]);
                    return $p;
                default:
                    return null;
            }
        }catch (ApiErrorException $exception) {
            return $exception->getMessage();
        }
    }

    public function deleteSetupPayment($stripe_id)
    {
        \Stripe\Stripe::setApiKey($this->key);

        try {
            $pm = PaymentMethod::retrieve($stripe_id);
            $pm->detach();
            return $pm;
        } catch (ApiErrorException $e) {
            return $e->getMessage();
        }
    }
}
