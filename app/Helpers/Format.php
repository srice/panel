<?php


namespace App\Helpers;


class Format
{
    public static function currencyFormat($amount)
    {
        return number_format($amount, 2, ',', ' ')." €";
    }
}
