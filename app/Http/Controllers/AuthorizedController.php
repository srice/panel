<?php

namespace App\Http\Controllers;

use App\Model\Comite\Payment;
use App\Notifications\AuthorizedNotification;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FA\Google2FA;

class AuthorizedController extends Controller
{
    public function authorized(Request $request)
    {
        $totp = new Google2FA();
        switch ($request->type)
        {
            case 'confirmVerifiedPayment':
                try {
                    $verify = $totp->verify($request->code, auth()->user()->google2fa_secret);
                    if($verify == true){
                        Payment::find($request->intent)->update([
                            "verified" => 1
                        ]);
                        auth()->user()->notify(new AuthorizedNotification('Confirmation d\'un mode de paiement', 'success', 'Le mode de paiement à été confirmer', 'la la-key'));
                        return response()->json(null, 201);
                    } else {
                        return response()->json(null, 202);
                    }
                } catch (IncompatibleWithGoogleAuthenticatorException $e) {
                    return response()->json($e->getMessage(), 420);
                } catch (InvalidCharactersException $e) {
                    return response()->json($e->getMessage(), 420);
                } catch (SecretKeyTooShortException $e) {
                    return response()->json($e->getMessage(), 420);
                }
                break;

            default: return response()->json("Invalid Route Parameter", 500);
        }
    }
}
