<?php

namespace App\Http\Controllers\Prestation;

use App\Http\Controllers\Controller;
use App\Model\Prestation\ServiceTarif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ServiceTarifController extends Controller
{
    /**
     * @var ServiceTarif
     */
    private $serviceTarif;

    /**
     * ServiceTarifController constructor.
     * @param ServiceTarif $serviceTarif
     */
    public function __construct(ServiceTarif $serviceTarif)
    {
        $this->serviceTarif = $serviceTarif->newQuery();
    }

    public function store($service_id, Request $request)
    {
        try {
            $request->validate([
                "name" => "required",
                "amount" => "required"
            ]);
        }catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de validation"
            ], 422);
        }

        $tarif = $this->serviceTarif->create([
            "service_id" => $service_id,
            "name" => $request->get('name'),
            "amount" => $request->get('amount'),
        ]);

        Log::info("Création du tarif: ".$tarif->name." pour le service: ".$tarif->service->name);
        return response()->json($tarif->toArray(), 201);
    }

    public function delete($service_id, $tarif_id)
    {
        $tarif = $this->serviceTarif->find($tarif_id);

        try {
            $tarif->delete();
            Log::info("Suppression du tarif: ".$tarif->name." pour le service: ".$tarif->service->name);
            return redirect()->back()->with('success', "Le tarif <strong>".$tarif->name."</strong> à été supprimé");
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->back()->with('error', 'Erreur lors de la suppression du tarif de service');
        }
    }
}
