<?php

namespace App\Http\Controllers\Prestation;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Module;
use App\Model\Prestation\ModuleChangelog;
use App\Model\Prestation\ModuleTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ModuleController extends Controller
{
    /**
     * @var Module
     */
    private $module;
    /**
     * @var ModuleTask
     */
    private $moduleTask;
    /**
     * @var ModuleChangelog
     */
    private $moduleChangelog;

    /**
     * ModuleController constructor.
     * @param Module $module
     * @param ModuleTask $moduleTask
     * @param ModuleChangelog $moduleChangelog
     */
    public function __construct(Module $module, ModuleTask $moduleTask, ModuleChangelog $moduleChangelog)
    {
        $this->module = $module->newQuery();
        $this->moduleTask = $moduleTask->newQuery();
        $this->moduleChangelog = $moduleChangelog->newQuery();
    }

    public function index()
    {
        return view("Prestation.Module.index", [
            "modules" => $this->module->get(),
            "tasks" => $this->moduleTask->get()
        ]);
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required",
                "version" => "required",
                "release" => "required"
            ]);
        } catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de Validation",
                "errors" => $exception->errors()
            ], 422);
        }

        try {
            $module = $this->module->create([
                "name" => $request->get('name'),
                "version" => $request->get('version'),
                "release" => $request->get('release'),
            ]);
            Log::info("Un module à été ajouté: " . $module->name);
            return response()->json($module->toArray(), 201);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json("Erreur 500: Erreur de script", 500);
        }
    }

    public function show($module_id)
    {
        return view("Prestation.Module.show", [
            "module" => $this->module->find($module_id),
            "taskDraft" => $this->moduleTask->where('module_id', $module_id)
                ->where('state', '0')
                ->orWhere('state', '1')
                ->orWhere('state', '3')
                ->count(),
            "taskComplete" => $this->moduleTask->where('module_id', $module_id)
                ->where('state', '2')
                ->count(),
        ]);
    }

    public function uploadLogo($module_id, Request $request)
    {
        if (Storage::disk('public')->exists('module/' . $module_id . '.png') == true) {
            try {
                Storage::disk('public')->delete('module/' . $module_id . '.png');
                try {
                    $request->file('file_logo')->storeAs('module/', $module_id . '.png', 'public');
                    try {
                        Storage::disk('public')->setVisibility('module/' . $module_id . '.png', 'public');
                        Log::info("Le logo du module à été définie");
                        return redirect()->back()->with('success', "Le logo du module à été définie");
                    } catch (FileException $exception) {
                        Log::error($exception->getMessage());
                        return redirect()->back()->with('error', "Erreur: " . $exception->getMessage());
                    }
                } catch (FileException $exception) {
                    Log::error($exception->getMessage());
                    return redirect()->back()->with('error', "Erreur: " . $exception->getMessage());
                }
            } catch (FileException $exception) {
                Log::error($exception->getMessage());
                return redirect()->back()->with('error', "Erreur: " . $exception->getMessage());
            }
        } else {
            try {
                $request->file('file_logo')->storeAs('module/', $module_id . '.png', 'public');
                try {
                    Storage::disk('public')->setVisibility('module/' . $module_id . '.png', 'public');
                    Log::info("Le logo du module à été définie");
                    return redirect()->back()->with('success', "Le logo du module à été définie");
                } catch (FileException $exception) {
                    Log::error($exception->getMessage());
                    return redirect()->back()->with('error', "Erreur: " . $exception->getMessage());
                }
            } catch (FileException $exception) {
                Log::error($exception->getMessage());
                return redirect()->back()->with('error', "Erreur: " . $exception->getMessage());
            }
        }
    }

    public function update($module_id, Request $request)
    {
        try {
            $request->validate([
                "name" => "required",
                "version" => "required",
                "release" => "required"
            ]);
        }catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de validation"
            ], 422);
        }

        try {
            $module = $this->module->find($module_id);
            $module->name = $request->get('name');
            $module->version = $request->get('version');
            $module->release = $request->get('release');
            $module->save();

            Log::info("Edition du module: ".$module->name);
            return response()->json($module->toArray(), 201);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json(null, 500);
        }
    }
    public function updateDesc($module_id, Request $request)
    {
        try {
            $module = $this->module->find($module_id);
            $module->description = $request->get('description');
            $module->save();

            Log::info("Edition de la description du module: ".$module->name);
            return response()->json($module->toArray(), 201);
        }catch (\Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json(null, 500);
        }
    }


}
