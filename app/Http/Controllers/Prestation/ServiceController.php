<?php

namespace App\Http\Controllers\Prestation;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Famille;
use App\Model\Prestation\Module;
use App\Model\Prestation\Service;
use App\Model\Prestation\ServiceTarif;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ServiceController extends Controller
{
    /**
     * @var Service
     */
    private $service;
    /**
     * @var ServiceTarif
     */
    private $serviceTarif;
    /**
     * @var Famille
     */
    private $famille;
    /**
     * @var Module
     */
    private $module;

    /**
     * ServiceController constructor.
     * @param Service $service
     * @param ServiceTarif $serviceTarif
     * @param Famille $famille
     * @param Module $module
     */
    public function __construct(Service $service, ServiceTarif $serviceTarif, Famille $famille, Module $module)
    {
        $this->service = $service->newQuery();
        $this->serviceTarif = $serviceTarif->newQuery();
        $this->famille = $famille->newQuery();
        $this->module = $module->newQuery();
    }

    public function index()
    {
        return view("Prestation.Service.index", [
            "familles" => $this->famille->get(),
            "services" => $this->service->get()->load('tarifs', 'famille'),
            "modules" => $this->module->get()
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->all());
        try {
            $request->validate([
                "famille_id" => "required",
                "name"      => "required",
                "kernel"    => "required"
            ]);
        }catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de Validation"
            ], 422);
        }
        if($request->module_id != null){$module_id = $request->module_id;}else{$module_id = null;}

        $service = $this->service->create([
            "famille_id" => $request->famille_id,
            "name"      => $request->name,
            "kernel"    => $request->kernel,
            "module_id" => $module_id
        ]);

        $tarif = $this->serviceTarif->create([
            "service_id" => $service->id,
            "name" => $request->name_tarif,
            "amount" => $request->montant_tarif,
        ]);

        Log::info("Création du service: ".$service->name);
        return response()->json($service->toArray(), 201);
    }

    public function show($service_id)
    {
        return view("Prestation.Service.show", [
            "service" => $this->service->find($service_id)
        ]);
    }

    public function edit($service_id)
    {
        return view("Prestation.Service.edit", [
            "familles" => $this->famille->get(),
            "service" => $this->service->find($service_id)
        ]);
    }

    public function update($service_id, Request $request)
    {
        try {
            $request->validate([
                "famille_id" => "required",
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de validation"
            ], 422);
        }

        $service = $this->service->find($service_id)
            ->update([
                "famille_id" => $request->get('famille_id'),
                "name" => $request->get('name')
            ]);

        $service = $this->service->find($service_id);

        Log::info("Edition du service: ".$service->name);
        return response()->json($service->toArray(), 201);
    }

    public function delete($service_id) {
        $service = $this->service->find($service_id);

        try {
            $service->delete();
            Log::info('Suppression du service: '.$service->name);
            return redirect('/prestation/service')->with('success', "Le service <strong>".$service->name."</strong> à été supprimer");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect('/prestation/service')->with('error', "Erreur lors de la suppression du service");
        }
    }
}
