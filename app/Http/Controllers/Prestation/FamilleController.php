<?php

namespace App\Http\Controllers\Prestation;

use App\Http\Controllers\Controller;
use App\Model\Prestation\Famille;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class FamilleController extends Controller
{
    /**
     * @var Famille
     */
    private $famille;

    /**
     * FamilleController constructor.
     * @param Famille $famille
     */
    public function __construct(Famille $famille)
    {
        $this->famille = $famille->newQuery();
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                "name" => "required"
            ]);
        }catch (ValidationException $exception) {
            Log::warning($exception->getMessage());
            return response()->json([
                "data" => "Erreur de Validation"
            ], 422);
        }

        $famille = $this->famille->create([
            "name" => $request->name
        ]);

        Log::info("Création de la famille de service: ".$famille->name);
        return response()->json($famille->toArray(), 201);
    }

    public function delete($famille_id)
    {
        $famille = $this->famille->find($famille_id);
        $count = $famille->services->count();

        if($count == 0){
            $famille->delete();
            Log::info("Suppression de la famille: ".$famille->name);
            return response()->json($famille->toArray(), 201);
        }else{
            Log::error("Impossible de supprimer cette famille car elle comporte des services (".$count.")");
            return response()->json($famille->toArray(), 203);
        }
    }
}
