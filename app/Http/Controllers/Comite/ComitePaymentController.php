<?php

namespace App\Http\Controllers\Comite;

use App\Helpers\Stripe;
use App\Http\Controllers\Controller;
use App\Mail\Comite\ReminderPayment;
use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\Model\Comite\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class ComitePaymentController extends Controller
{
    /**
     * @var Payment
     */
    private $payment;
    /**
     * @var Comite
     */
    private $comite;
    /**
     * @var Stripe
     */
    private $stripe;
    /**
     * @var Contact
     */
    private $contact;

    /**
     * ComitePaymentController constructor.
     * @param Payment $payment
     * @param Comite $comite
     * @param Stripe $stripe
     * @param Contact $contact
     */
    public function __construct(Payment $payment, Comite $comite, Stripe $stripe, Contact $contact)
    {
        $this->payment = $payment->newQuery();
        $this->comite = $comite->newQuery();
        $this->stripe = $stripe;
        $this->contact = $contact->newQuery();
    }

    public function create($comite_id)
    {
        return view("Comite.Payment.create", [
            "comite" => $this->comite->find($comite_id)
        ]);
    }

    public function store(Request $request, $comite_id)
    {
        try {
            $request->validate([]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $comite = $this->comite->find($comite_id);
        $setupPayment = $this->stripe->createSetupPayment(2, $comite->customerId, null, null, null, null, $request->iban, $comite);

        return response()->json(["iban" => $request->iban], 201);
    }

    public function delete($payment_id)
    {
        $payment = $this->payment->find($payment_id);
        $pm = $this->stripe->deleteSetupPayment($payment->stripe_id);
        try {
            $payment->delete();
        } catch (\Exception $e) {
            return redirect()->back(500);
        }

        return redirect()->back();
    }

    public function reminder($comite_id)
    {
        $comite = $this->comite->find($comite_id);
        $contacts = $this->contact->where('comite_id', $comite_id)->get()->load('user');
        foreach ($contacts as $contact) {
            Mail::to($contact->user->email)->send(new ReminderPayment($comite));
        }

        return response()->json(null, 200);
    }
}
