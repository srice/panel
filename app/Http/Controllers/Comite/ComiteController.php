<?php

namespace App\Http\Controllers\Comite;

use App\Http\Controllers\Controller;
use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\Model\Comite\Payment;
use App\Notifications\Comite\CreateComite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Stripe\Card;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;
use Stripe\Stripe;

class ComiteController extends Controller
{
    /**
     * @var Comite
     */
    private $comite;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Contact
     */
    private $contact;
    /**
     * @var Payment
     */
    private $payment;
    /**
     * @var \App\Helpers\Stripe
     */
    private $stripe;

    /**
     * ComiteController constructor.
     * @param Comite $comite
     * @param User $user
     * @param Contact $contact
     * @param Payment $payment
     * @param \App\Helpers\Stripe $stripe
     */
    public function __construct(Comite $comite, User $user, Contact $contact, Payment $payment, \App\Helpers\Stripe $stripe)
    {

        $this->comite = $comite->newQuery();
        $this->user = $user->newQuery();
        $this->contact = $contact->newQuery();
        $this->payment = $payment->newQuery();
        $this->stripe = $stripe;
    }

    public function index()
    {
        $comites = $this->comite->get();
        return view("Comite.index", [
            "comites" => $comites
        ]);
    }

    public function create()
    {
        return view("Comite.create");
    }

    public function store(Request $request) {
        $password = Str::random(8);
        try {
            $request->validate([
                "name" => "required",
                "adresse" => "required",
                "codePostal" => "required",
                "ville" => "required",
                "tel" => "required",
                "email" => "required|email"
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                "data" => "Erreur de Validation"
            ], 422);
        }

        $customer = $this->stripe->createCustomer(
            $request->name,
            $request->email,
            $request->adresse,
            $request->codePostal,
            $request->ville,
            $request->tel
        );

        $comite = $this->comite->create([
            "name" => $request->name,
            "adresse" => $request->adresse,
            "codePostal" => $request->codePostal,
            "ville" => $request->ville,
            "tel" => $request->tel,
            "email" => $request->email,
            "customerId" => $customer->id
        ]);

        $user = $this->user->create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($password)
        ]);
        $user->notify(new CreateComite($comite, $password));

        $contact = $this->contact->create([
            "comite_id" => $comite->id,
            "user_id" => $user->id,
            "position" => $request->position,
            "telephone" => $request->telephone
        ]);

        $setupPayment = $this->stripe->createSetupPayment($request->methode, $customer->id, $request->cbnumber, $request->exp_month, $request->exp_year, $request->cvv, $request->iban, $comite);

        return response()->json($comite->toArray(), 201);
    }

    public function show($comite_id)
    {
        //dd($this->comite->find($comite_id)->load('payments'));
        return view("Comite.show", [
            "comite" => $this->comite->find($comite_id)->load('contacts', 'payments')
        ]);
    }

    public function update(Request $request, $comite_id)
    {
        $comite = $this->comite->find($comite_id);
        if($request->exists('name') == true){$comite->name = $request->name;}
        if($request->exists('adresse') == true){$comite->adresse = $request->adresse;}
        if($request->exists('codePostal') == true){$comite->codePostal = $request->codePostal;}
        if($request->exists('ville') == true){$comite->ville = $request->ville;}
        if($request->exists('tel') == true){$comite->tel = $request->tel;}
        if($request->exists('email') == true){$comite->email = $request->email;}
        $comite->save();

        return response()->json($comite->toArray(), 201);
    }

    public function delete($comite_id)
    {
        $comite = $this->comite->find($comite_id);
        $comite->delete();

        return response()->json($comite->toArray(), 201);
    }
}
