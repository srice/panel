<?php

namespace App\Http\Controllers\Comite;

use App\Http\Controllers\Controller;
use App\Model\Comite\Comite;
use App\Model\Comite\Contact;
use App\Notifications\Comite\CreateContact;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ComiteContactController extends Controller
{
    /**
     * @var Contact
     */
    private $contact;
    /**
     * @var Comite
     */
    private $comite;
    /**
     * @var User
     */
    private $user;

    /**
     * ComiteContactController constructor.
     * @param Contact $contact
     * @param Comite $comite
     * @param User $user
     */
    public function __construct(Contact $contact, Comite $comite, User $user)
    {
        $this->contact = $contact->newQuery();
        $this->comite = $comite->newQuery();
        $this->user = $user->newQuery();
    }

    public function create($comite_id)
    {
        return view("Comite.Contact.create", [
            "comite" => $this->comite->find($comite_id)
        ]);
    }

    public function store(Request $request, $comite_id)
    {
        $password = Str::random(8);
        try {
            $request->validate([
                "name" => "required",
                "email" => "required|email|unique:users",
                "position" => "required",
                "telephone" => "required"
            ]);
        }catch (ValidationException $exception) {
            return response()->json([
                "exception" => $exception->getMessage(),
                "errors" => $exception->errors()
            ], 422);
        }

        $user = $this->user->create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($password)
        ]);

        $contact = $this->contact->create([
            "comite_id" => $comite_id,
            "user_id" => $user->id,
            "position" => $request->position,
            "telephone" => $request->telephone
        ]);

        $user->notify(new CreateContact($contact, $user, $password));

        return response()->json($contact->toArray(), 201);
    }

    public function edit($comite_id, $contact_id)
    {
        return view('Comite.Contact.edit', [
            "comite" => $this->comite->find($comite_id),
            "contact" => $this->contact->find($contact_id)
        ]);
    }

    public function update(Request $request, $comite_id, $contact_id)
    {
        $contact = $this->contact->find($contact_id);
        if($request->exists('position') == true){$contact->position = $request->position;}
        if($request->exists('telephone') == true){$contact->telephone = $request->telephone;}
        $contact->save();

        $user = $this->user->find($contact->user_id);
        if($request->exists('name') == true){$user->name = $request->name;}
        if($request->exists('email') == true){$user->email = $request->email;}
        $user->save();

        return response()->json([
            "contact" => $contact->toArray(),
            "user" => $user->toArray()
        ], 201);
    }

    public function delete($comite_id, $contact_id)
    {
        $contact = $this->contact->find($contact_id);
        $contact->delete();

        $user = $this->user->find($contact->user_id);
        $user->delete();

        return redirect()->back();
    }
}
