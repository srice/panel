<?php


namespace App\Packages\Srice;


use Illuminate\Support\Facades\Http;

class Srice
{
    private $endpoint;

    public function __construct()
    {
        if (env("APP_ENV") == "local") {
            $this->endpoint = "https://api.srice.io/api/";
        } else {
            $this->endpoint = "https://api.srice.eu/api/";
        }
    }

    public function get($uri, $params = null)
    {
        $data = Http::get($this->endpoint.$uri, $params);

        return response()->json($data->json());
    }

    public function post($uri, $params = null)
    {
        $data = Http::post($this->endpoint.$uri, $params);

        return response()->json($data->json());
    }

    public function delete($uri)
    {
        $data = Http::delete($this->endpoint.$uri);

        return response()->json($data->json());
    }
}
