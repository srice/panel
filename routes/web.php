<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);

Route::group(["middleware" => ["auth"]], function (){
    Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);

    Route::group(["prefix" => "comite", "namespace" => "Comite"], function (){
        Route::get('/', ["as" => "Comite.index", "uses" => "ComiteController@index"]);
        Route::get('create', ["as" => "Comite.create", "uses" => "ComiteController@create"]);
        Route::post('create', ["as" => "Comite.store", "uses" => "ComiteController@store"]);
        Route::get('{comite_id}', ["as" => "Comite.show", "uses" => "ComiteController@show"]);
        Route::put('{comite_id}', ["as" => "Comite.update", "uses" => "ComiteController@update"]);
        Route::get('{comite_id}/delete', ["as" => "Comite.delete", "uses" => "ComiteController@delete"]);

        Route::group(["prefix" => "{comite_id}/contact"], function (){
            Route::get('create', ["as" => "Comite.Contact.create", "uses" => "ComiteContactController@create"]);
            Route::post('create', ["as" => "Comite.Contact.store", "uses" => "ComiteContactController@store"]);
            Route::get('{contact_id}', ["as" => "Comite.Contact.edit", "uses" => "ComiteContactController@edit"]);
            Route::put('{contact_id}', ["as" => "Comite.Contact.update", "uses" => "ComiteContactController@update"]);
            Route::get('{contact_id}/delete', ["as" => "Comite.Contact.delete", "uses" => "ComiteContactController@delete"]);
        });

        Route::group(["prefix" => "{comite_id}/payment"], function (){
            Route::get('create', ["as" => "Comite.Payment.create", "uses" => "ComitePaymentController@create"]);
            Route::post('create', ["as" => "Comite.Payment.store", "uses" => "ComitePaymentController@store"]);
            Route::get('reminder', ["as" => "Comite.Payment.reminder", "uses" => "ComitePaymentController@reminder"]);
            Route::post('reminder', ["as" => "Comite.Payment.reminderAuthorize", "uses" => "ComitePaymentController@reminder"]);
            Route::get('{payment_id}/delete', ["as" => "Comite.Payment.delete", "uses" => "ComitePaymentController@delete"]);
        });
    });

    Route::group(["prefix" => "prestation", "namespace" => "Prestation"], function (){

        Route::group(["prefix" => "famille"], function (){
            Route::post('create', ["as" => "Service.Famille.store", "uses" => "FamilleController@store"]);
            Route::get('{famille_id}/delete', ["as" => "Service.Famille.delete", "uses" => "FamilleController@delete"]);
        });

        Route::group(["prefix" => "service"], function (){
            Route::get('/', ["as" => "Prestation.Service.index", "uses" => "ServiceController@index"]);
            Route::post('create', ["as" => "Prestation.Service.store", "uses" => "ServiceController@store"]);
            Route::get('{service_id}', ["as" => "Prestation.Service.show", "uses" => "ServiceController@show"]);
            Route::get('{service_id}/edit', ["as" => "Prestation.Service.edit", "uses" => "ServiceController@edit"]);
            Route::put('{service_id}/edit', ["as" => "Prestation.Service.update", "uses" => "ServiceController@update"]);
            Route::get('{service_id}/delete', ["as" => "Prestation.Service.delete", "uses" => "ServiceController@delete"]);

            Route::group(["prefix" => "{service_id}/tarif"], function () {
                Route::post('/', ["as" => "Service.Tarif.store", "uses" => "ServiceTarifController@store"]);
                Route::get('{tarif_id}', ["as" => "Service.Tarif.delete", "uses" => "ServiceTarifController@delete"]);
            });
        });

        Route::group(["prefix" => "module"], function (){
            Route::get('/', ["as" => "Prestation.Module.index", "uses" => "ModuleController@index"]);
            Route::post('/', ["as" => "Prestation.Module.store", "uses" => "ModuleController@store"]);
            Route::get('{module_id}', ["as" => "Prestation.Module.show", "uses" => "ModuleController@show"]);
            Route::post('{module_id}/edit_logo', ["as" => "Prestation.Module.uploadLogo", "uses" => "ModuleController@uploadLogo"]);
            Route::put('{module_id}', ["as" => "Prestation.Module.update", "uses" => "ModuleController@update"]);
            Route::post('{module_id}/edit_desc', ["as" => "Prestation.Module.updateDesc", "uses" => "ModuleController@updateDesc"]);
            Route::get('{module_id}/delete', ["as" => "Prestation.Module.delete", "uses" => "ModuleController@delete"]);
        });
    });
});

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');
Route::get('/code', 'TestController@code');
Route::get('/complete-registration', 'Auth\RegisterController@completeRegistration');

Route::post('/authorized', ["as" => "authorized", "uses" => "AuthorizedController@authorized"]);
