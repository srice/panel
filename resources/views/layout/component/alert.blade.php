@if($errors->any())
    <div class="alert alert-custom alert-outline-2x alert-outline-warning fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon-warning"></i>
        </div>
        <div class="alert-text">
            <h4 class="alert-heading">Erreur !</h4>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">
					<i class="ki ki-close"></i>
				</span>
            </button>
        </div>
    </div>
@endif

@if($message = session('success'))
    <div class="alert alert-custom alert-outline-2x alert-outline-success fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-check-mark"></i>
        </div>
        <div class="alert-text">{!! $message !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">
					<i class="ki ki-close"></i>
				</span>
            </button>
        </div>
    </div>
@endif

@if($message = session('warning'))
    <div class="alert alert-custom alert-outline-2x alert-outline-warning fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-warning"></i>
        </div>
        <div class="alert-text">{!! $message !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">
					<i class="ki ki-close"></i>
				</span>
            </button>
        </div>
    </div>
@endif

@if($message = session('error'))
    <div class="alert alert-custom alert-outline-2x alert-outline-danger fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-cross"></i>
        </div>
        <div class="alert-text">{!! $message !!}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">
					<i class="ki ki-close"></i>
				</span>
            </button>
        </div>
    </div>
@endif
