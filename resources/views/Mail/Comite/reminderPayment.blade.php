@extends("Mail.layout.mail")
<table width="450" border="0" cellspacing="0" cellpadding="0" class="container">
    <tr>
        <td height="50" align="center" valign="middle" class="mobile"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20"></td>
                    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td style="font-family: Montserrat, Arial, sans-serif; font-size: 20px; line-height:30px; letter-spacing:1px; color: #333333; font-weight:400;" data-color="H3 Color" data-size="H3 size" data-min="14" data-max="35" mc:edit=""><multiline>[SRICE] - Message Important</multiline></td>
                            </tr>
                            <tr>
                                <td align="center" height="20"></td>
                            </tr>
                            <tr>
                                <td align="left" class="mobile" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;" data-color="H7 Color" data-size="H7 Size" data-min="10" data-max="24" mc:edit="">
                                    <multiline label="content">
                                        <strong>Bonjour,</strong> {{ $comite->name }},<br>
                                        <p>Afin de continuer à utiliser nos services, un moyen de paiement est obligatoire afin d'effectuer le paiement de vos factures quel soit mensuel, trimestriel ou annuel.</p>
                                        <p>Notre système nous indique de votre compte ne comporte pas de moyen de paiement par défault.<br>Par conséquent nous vous invitons à enregistrer un moyen de paiement valide (CB ou RIB) par l'intermédiaire de votre espace client ou par téléphone au 0899 492 648.</p>
                                        <p>Cordialement,</p>
                                    </multiline>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="50"></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-family: OpenSans, Arial, sans-serif; font-size: 13px; color: #8D8D8D; line-height:23px; font-weight:400;" data-color="H7 Color" data-size="H7 Size" data-min="10" data-max="24" mc:edit="" class="mobileOn"><multiline label='content'>L'équipe commercial de Srice</multiline></td>
                            </tr>
                            </tbody>
                        </table></td>
                    <td width="20"></td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td height="50" align="center" valign="middle" class="mobile"></td>
    </tr>
</table>
