@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Liste des Modules</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Modules</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Liste des Modules</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <!--<a href="" class="btn btn-light font-weight-bold btn-sm"><i class="flaticon2-left-arrow"></i> Retour</a>-->
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="card card-custom bg-light card-stretch gutter-b">
                <!--begin::Body-->
                <div class="card-body">
					<span class="svg-icon svg-icon-dark svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2020-08-03-114926/theme/html/demo1/dist/../src/media/svg/icons/Layout/Layout-4-blocks.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"/>
                                <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon-->
                    </span>
                    <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $modules->count() }}</span>
                    <span class="font-weight-bold text-muted font-size-sm">Nombre total de modules</span>
                </div>
                <!--end::Body-->
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-custom bg-light card-stretch gutter-b">
                <!--begin::Body-->
                <div class="card-body">
					<span class="svg-icon svg-icon-dark svg-icon-2x">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M3,10.0500091 L3,8 C3,7.44771525 3.44771525,7 4,7 L9,7 L9,9 C9,9.55228475 9.44771525,10 10,10 C10.5522847,10 11,9.55228475 11,9 L11,7 L21,7 C21.5522847,7 22,7.44771525 22,8 L22,10.0500091 C20.8588798,10.2816442 20,11.290521 20,12.5 C20,13.709479 20.8588798,14.7183558 22,14.9499909 L22,17 C22,17.5522847 21.5522847,18 21,18 L11,18 L11,16 C11,15.4477153 10.5522847,15 10,15 C9.44771525,15 9,15.4477153 9,16 L9,18 L4,18 C3.44771525,18 3,17.5522847 3,17 L3,14.9499909 C4.14112016,14.7183558 5,13.709479 5,12.5 C5,11.290521 4.14112016,10.2816442 3,10.0500091 Z M10,11 C9.44771525,11 9,11.4477153 9,12 L9,13 C9,13.5522847 9.44771525,14 10,14 C10.5522847,14 11,13.5522847 11,13 L11,12 C11,11.4477153 10.5522847,11 10,11 Z" fill="#000000" opacity="0.3" transform="translate(12.500000, 12.500000) rotate(-45.000000) translate(-12.500000, -12.500000) "/>
                            </g>
                        </svg>
                    </span>
                    <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $tasks->count() }}</span>
                    <span class="font-weight-bold text-muted font-size-sm">Nombre total de taches</span>
                </div>
                <!--end::Body-->
            </div>
        </div>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <span class="svg-icon svg-icon-dark svg-icon-2x"><!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2020-08-03-114926/theme/html/demo1/dist/../src/media/svg/icons/Layout/Layout-4-blocks.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"/>
                                <path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"/>
                            </g>
                        </svg><!--end::Svg Icon-->
                    </span>
                </span>
                <h3 class="card-label">
                    Liste des modules
                </h3>
            </div>
            <div class="card-toolbar">
                <a data-toggle="modal" href="#addModule" class="btn btn-sm btn-light-primary font-weight-bold">
                    <i class="flaticon2-plus"></i> Nouveau module
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless table-vertical-center">
                    <thead>
                        <tr>
                            <th class="p-0" style="width: 50px;"></th>
                            <th class="p-0" style="width: 200px;"></th>
                            <th class="p-0" style="width: 320px;"></th>
                            <th class="p-0" style="width: 100px;"></th>
                            <th class="p-0" style="width: 100px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($modules as $module)
                        <tr>
                            <td class="pl-0">
                                @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('module/'.$module->id.'.png') == true)
                                    <div class="symbol symbol-50 mr-3">
                                        <img src="/storage/module/{{ $module->id }}.png">
                                    </div>
                                @else
                                    <div class="symbol symbol-50 mr-3">
                                        <span class="symbol-label font-size-h5">{{ \Illuminate\Support\Str::limit($module->name, 1, null) }}</span>
                                    </div>
                                @endif
                            </td>
                            <td class="pl-0">
                                <strong>{{ $module->name }}</strong><br>
                                <small class="text-muted">V.{{ $module->version }}</small>
                            </td>
                            <td class="pl-0">
                                {!! $module->description !!}
                            </td>
                            <td class="pl-0">
                                {!! \App\Helpers\Service::labelModuleRelease($module->release) !!}
                            </td>
                            <td class="pl-0">
                                <a href="{{ route('Prestation.Module.show', $module->id) }}" class="btn btn-icon btn-outline-primary btn-sm" data-toggle="tooltip" data-title="Voir le module"><i class="fa fa-eye"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addModule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="flaticon2-plus text-info"></i> Nouveau module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddModule" action="{{ route('Prestation.Module.store') }}" class="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nom du module</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="">Version Actuel</label>
                            <input type="text" id="fieldVersion" class="form-control" name="version" required>
                        </div>
                        <div class="form-group">
                            <label for="">Etat du module</label>
                            <select class="form-control selectpicker" name="release" required>
                                <option value="">Selectionner un état</option>
                                <option value="0" data-content="<span class='text-danger'>ALPHA</span>">ALPHA</option>
                                <option value="1" data-content="<span class='text-warning'>BETA</span>">BETA</option>
                                <option value="2" data-content="<span class='text-info'>GAMMA</span>">GAMMA</option>
                                <option value="3" data-content="<span class='text-success'>LIVE</span>">LIVE</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @component('layout.component.btnSubmit')
                            @slot('btnId')
                                btnSubmitAddModule
                            @endslot
                        @endcomponent
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="js/prestation/module/index.js"></script>
@endsection
