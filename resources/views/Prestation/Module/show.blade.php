@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Module: <strong>{{ $module->name }}</strong></h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Modules</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Fiche du module: {{ $module->name }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <a href="{{ route('Prestation.Module.index') }}" class="btn btn-light font-weight-bold btn-sm mr-3"><i class="flaticon2-left-arrow"></i> Retour</a>
                <a href="{{ route('Prestation.Module.delete', $module->id) }}" class="btn btn-outline-danger font-weight-bold btn-sm"><i class="flaticon2-trash"></i> Supprimer le module</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-custom bg-light card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
					<span class="svg-icon svg-icon-dark svg-icon-4x">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M7,11 L15,11 C16.1045695,11 17,10.1045695 17,9 L17,8 L19,8 L19,9 C19,11.209139 17.209139,13 15,13 L7,13 L7,15 C7,15.5522847 6.55228475,16 6,16 C5.44771525,16 5,15.5522847 5,15 L5,9 C5,8.44771525 5.44771525,8 6,8 C6.55228475,8 7,8.44771525 7,9 L7,11 Z" fill="#000000" opacity="0.3"/>
                                <path d="M6,21 C7.1045695,21 8,20.1045695 8,19 C8,17.8954305 7.1045695,17 6,17 C4.8954305,17 4,17.8954305 4,19 C4,20.1045695 4.8954305,21 6,21 Z M6,23 C3.790861,23 2,21.209139 2,19 C2,16.790861 3.790861,15 6,15 C8.209139,15 10,16.790861 10,19 C10,21.209139 8.209139,23 6,23 Z" fill="#000000" fill-rule="nonzero"/>
                                <path d="M18,7 C19.1045695,7 20,6.1045695 20,5 C20,3.8954305 19.1045695,3 18,3 C16.8954305,3 16,3.8954305 16,5 C16,6.1045695 16.8954305,7 18,7 Z M18,9 C15.790861,9 14,7.209139 14,5 C14,2.790861 15.790861,1 18,1 C20.209139,1 22,2.790861 22,5 C22,7.209139 20.209139,9 18,9 Z" fill="#000000" fill-rule="nonzero"/>
                                <path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" fill="#000000" fill-rule="nonzero"/>
                            </g>
                        </svg>
                    </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $module->version }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Version actuel du module</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-custom bg-light card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
					<span class="svg-icon svg-icon-dark svg-icon-4x">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M3,10.0500091 L3,8 C3,7.44771525 3.44771525,7 4,7 L9,7 L9,9 C9,9.55228475 9.44771525,10 10,10 C10.5522847,10 11,9.55228475 11,9 L11,7 L21,7 C21.5522847,7 22,7.44771525 22,8 L22,10.0500091 C20.8588798,10.2816442 20,11.290521 20,12.5 C20,13.709479 20.8588798,14.7183558 22,14.9499909 L22,17 C22,17.5522847 21.5522847,18 21,18 L11,18 L11,16 C11,15.4477153 10.5522847,15 10,15 C9.44771525,15 9,15.4477153 9,16 L9,18 L4,18 C3.44771525,18 3,17.5522847 3,17 L3,14.9499909 C4.14112016,14.7183558 5,13.709479 5,12.5 C5,11.290521 4.14112016,10.2816442 3,10.0500091 Z M10,11 C9.44771525,11 9,11.4477153 9,12 L9,13 C9,13.5522847 9.44771525,14 10,14 C10.5522847,14 11,13.5522847 11,13 L11,12 C11,11.4477153 10.5522847,11 10,11 Z" fill="#000000" opacity="0.3" transform="translate(12.500000, 12.500000) rotate(-45.000000) translate(-12.500000, -12.500000) "/>
                            </g>
                        </svg>
                    </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $module->tasks->count() }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Nombre de tache total du module</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-custom bg-light-warning card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
					<span class="svg-icon svg-icon-warning svg-icon-4x">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3"/>
                                <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                            </g>
                        </svg>
                    </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $taskDraft }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Nombre de tache en cours...</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card card-custom bg-light-success card-stretch gutter-b">
                        <!--begin::Body-->
                        <div class="card-body">
					<span class="svg-icon svg-icon-success svg-icon-4x">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <polygon points="0 0 24 0 24 24 0 24"/>
                                <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002) "/>
                            </g>
                        </svg>
                    </span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $taskComplete }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Nombre de tache en terminer</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {!! \App\Helpers\Module::cardRelease($module->release) !!}
        </div>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <ul class="dashboard-tabs nav nav-pills nav-danger row row-paddingless m-0 p-0" role="tablist">
                <!--begin::Item-->
                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                    <a class="nav-link active border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#tab_general">
                            <span class="nav-icon py-2 w-auto">
                                <span class="svg-icon svg-icon-3x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z" fill="#000000"/>
                                        <rect fill="#000000" opacity="0.3" transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519) " x="16.3255682" y="2.94551858" width="3" height="18" rx="1"/>
                                    </g>
                                </svg><!--end::Svg Icon-->
                                </span>
                            </span>
                        <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Général</span>
                    </a>
                </li>
                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                    <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#tab_tasks">
                            <span class="nav-icon py-2 w-auto">
                                <span class="svg-icon svg-icon-3x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path d="M3,10.0500091 L3,8 C3,7.44771525 3.44771525,7 4,7 L9,7 L9,9 C9,9.55228475 9.44771525,10 10,10 C10.5522847,10 11,9.55228475 11,9 L11,7 L21,7 C21.5522847,7 22,7.44771525 22,8 L22,10.0500091 C20.8588798,10.2816442 20,11.290521 20,12.5 C20,13.709479 20.8588798,14.7183558 22,14.9499909 L22,17 C22,17.5522847 21.5522847,18 21,18 L11,18 L11,16 C11,15.4477153 10.5522847,15 10,15 C9.44771525,15 9,15.4477153 9,16 L9,18 L4,18 C3.44771525,18 3,17.5522847 3,17 L3,14.9499909 C4.14112016,14.7183558 5,13.709479 5,12.5 C5,11.290521 4.14112016,10.2816442 3,10.0500091 Z M10,11 C9.44771525,11 9,11.4477153 9,12 L9,13 C9,13.5522847 9.44771525,14 10,14 C10.5522847,14 11,13.5522847 11,13 L11,12 C11,11.4477153 10.5522847,11 10,11 Z" fill="#000000" opacity="0.3" transform="translate(12.500000, 12.500000) rotate(-45.000000) translate(-12.500000, -12.500000) "/>
                                        </g>
                                    </svg>
                                </span>
                            </span>
                        <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Taches</span>
                    </a>
                </li>
                <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                    <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#tab_changelogs">
                            <span class="nav-icon py-2 w-auto">
                                <span class="svg-icon svg-icon-3x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path d="M7,11 L15,11 C16.1045695,11 17,10.1045695 17,9 L17,8 L19,8 L19,9 C19,11.209139 17.209139,13 15,13 L7,13 L7,15 C7,15.5522847 6.55228475,16 6,16 C5.44771525,16 5,15.5522847 5,15 L5,9 C5,8.44771525 5.44771525,8 6,8 C6.55228475,8 7,8.44771525 7,9 L7,11 Z" fill="#000000" opacity="0.3"/>
                                            <path d="M6,21 C7.1045695,21 8,20.1045695 8,19 C8,17.8954305 7.1045695,17 6,17 C4.8954305,17 4,17.8954305 4,19 C4,20.1045695 4.8954305,21 6,21 Z M6,23 C3.790861,23 2,21.209139 2,19 C2,16.790861 3.790861,15 6,15 C8.209139,15 10,16.790861 10,19 C10,21.209139 8.209139,23 6,23 Z" fill="#000000" fill-rule="nonzero"/>
                                            <path d="M18,7 C19.1045695,7 20,6.1045695 20,5 C20,3.8954305 19.1045695,3 18,3 C16.8954305,3 16,3.8954305 16,5 C16,6.1045695 16.8954305,7 18,7 Z M18,9 C15.790861,9 14,7.209139 14,5 C14,2.790861 15.790861,1 18,1 C20.209139,1 22,2.790861 22,5 C22,7.209139 20.209139,9 18,9 Z" fill="#000000" fill-rule="nonzero"/>
                                            <path d="M6,7 C7.1045695,7 8,6.1045695 8,5 C8,3.8954305 7.1045695,3 6,3 C4.8954305,3 4,3.8954305 4,5 C4,6.1045695 4.8954305,7 6,7 Z M6,9 C3.790861,9 2,7.209139 2,5 C2,2.790861 3.790861,1 6,1 C8.209139,1 10,2.790861 10,5 C10,7.209139 8.209139,9 6,9 Z" fill="#000000" fill-rule="nonzero"/>
                                        </g>
                                    </svg>
                                </span>
                            </span>
                        <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Versionning</span>
                    </a>
                </li>
                <!--end::Item-->
            </ul>
        </div>
    </div>
    <div class="tab-content m-0 p-0">
        <div class="tab-pane active" id="tab_general">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-custom gutter-b">
                        <form class="form" action="{{ route('Prestation.Module.uploadLogo', $module->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body justify-content-center">
                                <div class="image-input image-input-outline" id="kt_image_1">
                                    @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('module/'.$module->id.'.png') == true)
                                        <div class="image-input-wrapper" style="background-image: url(/storage/module/{{ $module->id }}.png)"></div>
                                    @else
                                        <div class="image-input-wrapper" style="background-image: url(/storage/module/1.png)"></div>
                                    @endif
                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Changer le logo">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="file_logo" accept=".png, .jpg, .jpeg"/>
                                        <input type="hidden" name="profile_avatar_remove"/>
                                    </label>

                                    <span class="btn btn-xs btn-icon btn-circle btn-danger btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Annuler">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                                </div>
                            </div>
                            <div class="card-footer">
                                @component('layout.component.btnSubmit')
                                @endcomponent
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="flaticon2-edit text-primary"></i>
                                </span>
                                <h3 class="card-label">Edition du modules</h3>
                            </div>
                        </div>
                        <form id="formEditModule" action="{{ route('Prestation.Module.update', $module->id) }}">
                            @csrf
                            @method("PUT")
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Nom du module</label>
                                    <input type="text" class="form-control" name="name" value="{{ $module->name }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Version Actuel</label>
                                    <input type="text" id="fieldVersion" class="form-control" name="version" value="{{ $module->version }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Etat du module</label>
                                    <select class="form-control selectpicker" name="release" required>
                                        <option value="{{ $module->release }}">{{ \App\Helpers\Service::textModuleRelease($module->release) }}</option>
                                        <option value="0" data-content="<span class='text-danger'>ALPHA</span>">ALPHA</option>
                                        <option value="1" data-content="<span class='text-warning'>BETA</span>">BETA</option>
                                        <option value="2" data-content="<span class='text-info'>GAMMA</span>">GAMMA</option>
                                        <option value="3" data-content="<span class='text-success'>LIVE</span>">LIVE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-center">
                                @component('layout.component.btnSubmit')
                                    btnEditFormModule
                                @endcomponent
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-md-12">
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="flaticon2-edit text-primary"></i>
                                </span>
                                <h3 class="card-label">Edition de la description</h3>
                            </div>
                        </div>
                        <form id="formEditDescModule" action="{{ route('Prestation.Module.updateDesc', $module->id) }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div name="description" id="kt_quil_1" style="height: 325px">
                                    {!! $module->description !!}
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-center">
                                @component('layout.component.btnSubmit')
                                    btnEditDescFormModule
                                @endcomponent
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_tasks">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">
                            Liste des taches
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a data-toggle="modal" href="#addTask" class="btn btn-sm btn-light-primary font-weight-bold">
                            <i class="flaticon2-plus"></i> Nouvelle Tache
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless table-vertical-center" id="tableTasks">
                            <thead>
                            <tr>
                                <th class="p-0" style="width: 50px;">#</th>
                                <th class="p-0" style="width: 200px;">Sujet</th>
                                <th class="p-0" style="width: 120px;">Horodatage</th>
                                <th class="p-0" style="width: 120px;">Etat</th>
                                <th class="p-0" style="width: 100px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($module->tasks as $task)
                                <td>#{{ $task->id }}</td>
                                <td>{{ $task->subject }}</td>
                                <td>
                                    <strong>Début:</strong> {{ $task->start->format('d/m/Y à H:i') }}
                                    @if($task->end != null)
                                    <br>
                                    <strong>Fin:</strong> {{ $task->end->format('d/m/Y à H:i') }}
                                    @endif
                                </td>
                                <td>{!! \App\Helpers\Module::labelStateTask($task->state) !!}</td>
                                <td>
                                    <a href="" class="btn btn-icon btn-sm btn-outline-dark" data-toggle="tooltip" title="Voir la tache"><i class="fa fa-eye"></i> </a>
                                    <a href="" class="btn btn-icon btn-sm btn-outline-info" data-toggle="tooltip" title="Editer la tache"><i class="fa fa-edit"></i> </a>
                                    <a href="" class="btn btn-icon btn-sm btn-outline-danger" data-toggle="tooltip" title="Supprimer la Tache"><i class="fa fa-trash"></i> </a>
                                </td>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_changelogs">

        </div>
    </div>
    <div class="modal fade" id="addTask" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus-circle text-primary"></i> Nouvelle tache</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddModuleTask" class="form" action="" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Sujet</label>
                            <input type="text" class="form-control form-control-lg" name="subject" required>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <div name="description" id="quil_task_description" style="height: 125px"></div>
                            <input type="hidden" id="taskDescription" name="taskDescription">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="">Date de début</label>
                                <input type="text" class="form-control datepicker" name="start" required>
                            </div>
                            <div class="col-md-6">
                                <label for="">Date de fin</label>
                                <input type="text" class="form-control datepicker" name="end" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Etat de la tache</label>
                            <select name="state" id="stateTask" class="form-control selectpicker">
                                <option value=""></option>
                                <option value="0">Nouveau</option>
                                <option value="1">En cours</option>
                                <option value="2">Terminer</option>
                                <option value="3">En attente</option>
                                <option value="4">Annuler</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @component('layout.component.btnSubmit')
                            btnSubmitAddModuleTask
                        @endcomponent
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="js/prestation/module/show.js"></script>
@endsection
