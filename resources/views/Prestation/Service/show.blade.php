@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Service: <strong>{{ $service->name }}</strong></h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Services</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Liste des Services</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center flex-wrap">
                <!--begin::Actions-->
                <a href="{{ route('Prestation.Service.index') }}" class="btn btn-light font-weight-bold btn-sm mr-3"><i class="flaticon2-left-arrow"></i> Retour</a>
                <a href="{{ route('Prestation.Service.edit', $service->id) }}" class="btn btn-primary font-weight-bold btn-sm mr-3"><i class="flaticon2-edit"></i> Editer</a>
                <a href="{{ route('Prestation.Service.delete', $service->id) }}" class="btn btn-danger font-weight-bold btn-sm mr-3"><i class="flaticon2-trash"></i> Supprimer</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <!--begin::Body-->
        <div class="card-body pt-4">
            <!--begin::User-->
            <div class="d-flex align-items-center">
                <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                    <div class="symbol-label" style="background-image:url('/storage/icons/call-center-agent.png')"></div>
                    <i class="symbol-badge bg-success"></i>
                </div>
                <div>
                    <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">{{ $service->name }}</a>
                    <div class="text-muted">{{ $service->famille->name }}</div>
                </div>
            </div>
            <!--end::User-->
            <!--begin::Contact-->
            <div class="pt-8 pb-6">
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <span class="font-weight-bold mr-2">Type de Service:</span>
                    <span class="text-muted">{{ \App\Helpers\Service::stringTypeService($service->kernel) }}</span>
                </div>
                @if($service->kernel == 2)
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Module Affillier:</span>
                        <span class="text-muted">{{ $service->module->name }}</span>
                    </div>
                @endif
            </div>
            <!--end::Contact-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="fa fa-euro-sign text-primary"></i>
                </span>
                <h3 class="card-label">
                    Liste des tarifs
                </h3>
            </div>
            <div class="card-toolbar">
                <a data-toggle="modal" href="#addtarif" class="btn btn-sm btn-light-primary font-weight-bold">
                    <i class="flaticon2-plus"></i> Nouveau Tarif
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-12 col-xl-12">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="search_table_tarifs" />
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table class="datatable datatable-bordered datatable-head-custom" id="listeTarifs">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nom du tarif</th>
                    <th>Montant</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($service->tarifs as $tarif)
                    <tr>
                        <td>{{ $tarif->id }}</td>
                        <td>{{ $tarif->name }}</td>
                        <td>{{ \App\Helpers\Format::currencyFormat($tarif->amount) }}</td>
                        <td>
                            <a href="{{ route('Service.Tarif.delete', [$service->id, $tarif->id]) }}" class="btn btn-icon btn-outline-danger btn-sm"><i class="la la-trash-o"></i> </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal-->
    <div class="modal fade" id="addTarif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="flaticon2-plus text-info"></i> Nouveau Tarif</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form id="formAddTarif" action="{{ route('Service.Tarif.store', $service->id) }}" class="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nom du tarif</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="">Montant</label>
                            <input type="text" class="form-control" name="amount" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        @component('layout.component.btnSubmit')
                        @endcomponent
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="js/prestation/service/show.js"></script>
@endsection
