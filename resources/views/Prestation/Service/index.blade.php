@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Liste des Services</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Services</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Liste des Services</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <!--<a href="" class="btn btn-light font-weight-bold btn-sm"><i class="flaticon2-left-arrow"></i> Retour</a>-->
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <div class="card card-custom ">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Familles de service</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Dropdown-->
                <!--end::Dropdown-->
                <!--begin::Button-->
                <a data-toggle="modal" href="#addFamille" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>Nouvelle famille de service</a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="kt_datatable_search_query_famille" />
                                    <span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeFamille">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Nom</th>
                    <th title="Field #5">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($familles as $famille)
                <tr>
                    <td>{{ $famille->id }}</td>
                    <td>{{ $famille->name }}</td>
                    <td>
                        <a href="{{ route('Service.Famille.delete', $famille->id) }}" id="btnDeleteFamille" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Supprimer la famille"><i class="la la-times-circle"></i> </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <br>
    <div class="card card-custom ">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste des Services</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a data-toggle="modal" href="#addService" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>Nouveau services</a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Recherche..." id="kt_datatable_search_query_service" />
                                    <span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="listeServices">
                <thead>
                <tr>
                    <th title="Field #1">#</th>
                    <th title="Field #2">Famille</th>
                    <th title="Field #3">Désignation</th>
                    <th title="Field #4">Tarif</th>
                    <th title="Field #5">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                <tr>
                    <td>{{ $service->id }}</td>
                    <td>{{ $service->famille->name }}</td>
                    <td>{{ $service->name }}</td>
                    <td>{{ number_format($service->tarifs[0]->amount, 2, ',', ' ')." €" }}</td>
                    <td>
                        <a href="{{ route('Prestation.Service.show', $service->id) }}" class="btn btn-icon btn-primary" data-toggle="tooltip" title="Voir le service"><i class="la la-eye"></i> </a>
                        <a id="btnDeleteService" href="{{ route('Prestation.Service.delete', $service->id) }}" class="btn btn-icon btn-danger" data-toggle="tooltip" title="Supprimer le service"><i class="la la-trash-o"></i> </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <div id="addFamille" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nouvelle Famille de service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="{{ route('Service.Famille.store') }}" class="form" id="formAddFamille">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">Nom de la famille</label>
                            <input type="text" id="name" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                        <button type="submit" id="btnSubmitFormFamille" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="addService" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nouveau Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form action="{{ route('Prestation.Service.store') }}" class="form" id="formAddService">
                    @csrf
                    <div class="modal-body">
                        <h3 class="fc-title">1. Service</h3>
                        <div class="form-group">
                            <label for="name">Famille de Service</label>
                            <select class="form-control selectpicker" data-size="7" data-live-search="true" name="famille_id" required>
                                <option value="">Selectionner une famille</option>
                                @foreach($familles as $famille)
                                    <option value="{{ $famille->id }}">{{ $famille->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Nom du service *</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Type de service</label>
                            <select id="selectKernel" class="form-control selectpicker" name="kernel" required>
                                <option value="">Selectionner un type de service</option>
                                <option value="0">Aucun</option>
                                <option value="1">Espace</option>
                                <option value="2">Module</option>
                            </select>
                        </div>
                        <div id="divModule">
                            <div class="form-group">
                                <label>Module Associé</label>
                                <select name="module_id" id="listeModule" class="selectpicker form-control" data-live-search="true">
                                    <option value="">Selectionner un module</option>
                                    @foreach($modules as $module)
                                        <option value="{{ $module->id }}">{{ $module->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <hr />
                        <h3 class="fc-title">2. Tarification Principal</h3>
                        <div class="form-group">
                            <label for="name">Nom du tarif *</label>
                            <input type="text" class="form-control" name="name_tarif" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Montant du tarif *</label>
                            <input type="text" class="form-control" name="montant_tarif" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                        <button type="submit" id="btnSubmitFormService" class="btn btn-primary">Sauvegarder</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="js/prestation/service/index.js"></script>
@endsection
