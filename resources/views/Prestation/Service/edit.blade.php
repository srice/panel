@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Service: <strong>{{ $service->name }}</strong></h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Services</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Edition d'un service</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center flex-wrap">
                <!--begin::Actions-->
                <a href="{{ route('Prestation.Service.index') }}" class="btn btn-light font-weight-bold btn-sm mr-3"><i
                        class="flaticon2-left-arrow"></i> Retour</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
				<span class="card-icon">
					<i class="flaticon2-edit text-primary"></i>
				</span>
                <h3 class="card-label">Edition d'un service: {{ $service->name }}</h3>
            </div>
        </div>
        <form id="formEditService" action="{{ route('Prestation.Service.update', $service->id) }}" class="form">
            @method("PUT")
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="">Famille</label>
                    <select name="famille_id" class="form-control selectpicker" data-size="7" data-live-search="true">
                        <option value="{{ $service->famille_id }}">{{ $service->famille->name }}</option>
                        @foreach($familles as $famille)
                            <option value="{{ $famille->id }}">{{ $famille->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Nom du service</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ $service->name }}" required>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-center">
                @component('layout.component.btnSubmit')
                    @slot('btnId')
                        btnSubmitEditForm
                    @endslot
                @endcomponent
            </div>
        </form>
    </div>
@endsection

@section("script")
    <script src="/js/prestation/service/edit.js"></script>
@endsection
