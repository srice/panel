@extends('layout.app')

@section("style")
    <link href="assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css"/>
@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Création d'un comité</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Comité</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Création d'un comité</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <a href="{{ route('Comite.index') }}" class="btn btn-light font-weight-bold btn-sm"><i
                        class="flaticon2-left-arrow"></i> Retour</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <form id="formAddComite" class="form" action="{{ route('Comite.store') }}" method="post">
        @csrf
        <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-plus text-primary"></i>
                </span>
                    <h3 class="card-label">
                        Nouveau Comité
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmitForm" class="btn btn-primary font-weight-bolder"><i
                            class="ki ki-check icon-sm"></i>Sauvegarder
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <h3 class="text-dark font-weight-bold mb-10">Information du comité:</h3>
                        <div class="form-group row">
                            <label class="col-3">Nom du comité</label>
                            <input type="text" class="form-control col-9" name="name">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Adresse du comité</label>
                            <input type="text" class="form-control col-9" name="adresse">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Code Postal</label>
                            <input type="text" class="form-control col-3" name="codePostal">
                            <label class="col-2">Ville</label>
                            <input type="text" class="form-control col-4" name="ville">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Téléphone du comité</label>
                            <input type="tel" class="form-control col-9" name="tel">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Email du comité</label>
                            <input type="email" class="form-control col-9" name="email">
                        </div>
                        <div class="separator separator-dashed my-8"></div>
                        <h3 class="text-dark font-weight-bold mb-10">Contact principal du comité:</h3>
                        <div class="form-group row">
                            <label class="col-3">Position du contact</label>
                            <input type="text" class="form-control col-9" name="position">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">Téléphone du contact</label>
                            <input type="text" class="form-control col-9" name="telephone">
                        </div>
                        <div class="separator separator-dashed my-8"></div>
                        <h3 class="text-dark font-weight-bold mb-10">Mode de Paiement par default:</h3>
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="option option-plain">
                                <span class="option-control">
                                    <span class="radio">
                                        <input type="radio" name="methode" value="1"/>
                                        <span></span>
                                    </span>
                                </span>
                                    <span class="option-label">
                                    <span class="option-head">
                                        <span class="option-title">
                                            Carte Bancaire
                                        </span>
                                    </span>
                                </span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="option option-plain">
                                    <span class="option-control">
                                        <span class="radio">
                                            <input type="radio" name="methode" value="2"/>
                                            <span></span>
                                        </span>
                                    </span>
                                    <span class="option-label">
                                        <span class="option-head">
                                            <span class="option-title">
                                                Prélèvement Bancaire
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="option option-plain">
                                    <span class="option-control">
                                        <span class="radio">
                                            <input type="radio" name="methode" value="0" checked="checked"/>
                                            <span></span>
                                        </span>
                                    </span>
                                    <span class="option-label">
                                        <span class="option-head">
                                            <span class="option-title">
                                                Aucun
                                            </span>
                                        </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div id="form_cb">
                            <div class="form-group row">
                                <div class="col-6">
                                    <input type="text" class="form-control" name="cbnumber"
                                           placeholder="Numéro de la carte bancaire">
                                </div>
                                <div class="col-2">
                                    <input type="text" class="form-control" name="exp_month" placeholder="MM">
                                </div>
                                <div class="col-2">
                                    <input type="text" class="form-control" name="exp_year" placeholder="YY">
                                </div>
                                <div class="col-2">
                                    <input type="text" class="form-control" name="cvv" placeholder="CVV">
                                </div>
                            </div>
                        </div>
                        <div id="form_prlv">
                            <div class="form-group">
                                <label class="col-3">IBAN</label>
                                <input type="text" class="form-control" name="iban">
                            </div>
                            <div class="form-group">
                                <label class="col-3">BIC</label>
                                <input type="text" class="form-control" name="bic">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="js/comite/create.js"></script>
@endsection
