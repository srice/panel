@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Ajout d'un mode de paiement</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Comité</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">{{ $comite->name }}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Ajout d'un mode de paiement</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <a href="{{ route('Comite.show', $comite->id) }}" class="btn btn-light font-weight-bold btn-sm"><i
                        class="flaticon2-left-arrow"></i> Retour</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <form id="formAddPaiment" class="form" action="{{ route('Comite.Payment.store', $comite->id) }}" method="post">
        @csrf
        <div class="card card-custom gutter-b card-sticky" id="kt_page_sticky_card">
            <div class="card-header">
                <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-plus text-primary"></i>
                </span>
                    <h3 class="card-label">
                        Nouveau mode de paiement
                    </h3>
                </div>
                <div class="card-toolbar">
                    <button type="submit" id="btnSubmitForm" class="btn btn-primary font-weight-bolder"><i
                            class="ki ki-check icon-sm"></i>Sauvegarder
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">Seul le mode de paiement par Prélèvement Bancaire peut être renseigner par le service.</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="ki ki-close"></i></span>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <h3 class="text-dark font-weight-bold mb-10">Information du mode de paiement:</h3>
                        <div class="form-group row">
                            <label class="col-3">Iban</label>
                            <input type="text" class="form-control col-9" name="iban">
                        </div>
                        <div class="form-group row">
                            <label class="col-3">BIC</label>
                            <input type="text" class="form-control col-9" name="bic">
                        </div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/js/comite/paiment/create.js"></script>
@endsection
