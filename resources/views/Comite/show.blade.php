@extends('layout.app')

@section("style")

@endsection

@section("bread")
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-2 mr-5">Comité: {{ $comite->name }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Comité</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">Fiche du comité: {{ $comite->name }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                <a href="{{ route("Comite.index") }}" class="btn btn-light font-weight-bold btn-sm"><i class="flaticon2-left-arrow"></i> Retour</a>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section("content")
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="d-flex">
                <!--begin::Pic-->
                <div class="flex-shrink-0 mr-7">
                    <div class="symbol symbol-50 symbol-lg-120">
                        <img alt="Pic" src="assets/media/logos/logo-letter-4.png" />
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin: Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <!--begin::User-->
                        <div class="mr-3">
                            <div class="d-flex align-items-center mr-3">
                                <!--begin::Name-->
                                <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{ $comite->name }}</a>
                                <!--end::Name-->
                            </div>
                            <!--begin::Contacts-->
                            <div class="d-flex flex-wrap my-2">
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
															<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000" />
																		<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>{{ $comite->email }}</a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold">
															<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Map/Marker2.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24" />
																		<path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000" />
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>{{ $comite->adresse }} {{ $comite->codePostal }} {{ $comite->ville }}</a>
                            </div>
                            <!--end::Contacts-->
                        </div>
                        <!--begin::User-->
                        <!--begin::Actions-->
                        <div class="mb-10">
                            <a href="tel:{{ $comite->tel }}" class="btn btn-sm btn-light-skype btn-icon mr-2" data-toggle="tooltip" title="Appeler le comité"><i class="la la-phone"></i> </a>
                            <a href="mailto:{{ $comite->email }}" class="btn btn-sm btn-light-google btn-icon mr-2" data-toggle="tooltip" title="Ecrire au comité"><i class="la la-envelope"></i> </a>
                        </div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Title-->
                    <!--begin::Content-->
                    <div class="d-flex align-items-center flex-wrap justify-content-between">

                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Info-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            @if(count($comite->payments) == 0)
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 bg-danger p-8 card-rounded flex-grow-1 bgi-no-repeat" style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(assets/media/svg/logos/visa.svg)">

                            <h4 class="text-inverse-danger mt-2 font-weight-bolder">Mode de paiment</h4>

                            <p class="text-inverse-danger my-6">
                                Aucun mode de paiement n'est disponible pour ce client.
                            </p>

                            <a href="{{ route('Comite.Payment.reminder', $comite->id) }}" id="btnRemindDefaultPayment" class="btn btn-warning font-weight-bold py-2 px-6" data-toggle="popover" title="Information" data-content="Cliquez sur ce bouton pour envoyer une notification au client">Rappelez</a>
                        </div>
                    </div>
                </div>
            @endif

                <div class="card card-custom">
                    <!--begin::Header-->
                    <div class="card-header h-auto py-4">
                        <div class="card-title">
                            <h3 class="card-label">
                                Comité
                                <span class="d-block text-muted pt-2 font-size-sm">Information du comité</span>
                            </h3>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-4">
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Nom du comité:</label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comite->name }}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Adresse:</label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ $comite->adresse }}<br>{{ $comite->codePostal }} {{ $comite->ville }}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Coordonnée:</label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">
                                    <i class="flaticon2-phone"></i> {{ $comite->tel }}<br>
                                    <i class="flaticon-envelope"></i> {{ $comite->email }}
                                </span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">CA Client:</label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ \App\Helpers\Comite::sumCaForComite($comite->id) }}</span>
                            </div>
                        </div>
                        <div class="form-group row my-2">
                            <label class="col-4 col-form-label">Nombre de ticket:</label>
                            <div class="col-8">
                                <span class="form-control-plaintext font-weight-bolder">{{ \App\Helpers\Comite::countTicketForComite($comite->id) }}</span>
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
        </div>
        <div class="col-9">
            <div class="card card-custom">
                <div class="card-header card-header-tabs-line">
                    <div class="card-toolbar">
                        <ul class="nav nav-tabs nav-bold nav-tabs-line">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#general">
                                    <span class="nav-icon"><i class="flaticon2-edit"></i></span>
                                    <span class="nav-text">Edition</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#contact">
                                    <span class="nav-icon"><i class="flaticon2-user"></i></span>
                                    <span class="nav-text">Contacts</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#payment">
                                    <span class="nav-icon"><i class="la la-credit-card"></i></span>
                                    <span class="nav-text">Mode de Paiements</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general">
                            <form class="form" id="formEditComite" action="{{ route('Comite.update', $comite->id) }}" method="POST">
                                @csrf
                                @method("PUT")
                                <div class="form-group row">
                                    <label class="col-3">Nom du comité</label>
                                    <input type="text" class="form-control col-9" name="name" value="{{ $comite->name }}">
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Adresse du comité</label>
                                    <input type="text" class="form-control col-9" name="adresse" value="{{ $comite->adresse }}">
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Code Postal</label>
                                    <input type="text" class="form-control col-3" name="codePostal" value="{{ $comite->codePostal }}">
                                    <label class="col-2">Ville</label>
                                    <input type="text" class="form-control col-4" name="ville" value="{{ $comite->ville }}">
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Téléphone du comité</label>
                                    <input type="tel" class="form-control col-9" name="tel" value="{{ $comite->tel }}">
                                </div>
                                <div class="form-group row">
                                    <label class="col-3">Email du comité</label>
                                    <input type="email" class="form-control col-9" name="email" value="{{ $comite->email }}">
                                </div>
                                <button type="submit" id="btnUpdateForm" class="btn btn-primary">Mise à jour</button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact">
                            <div class="card card-custom card-transparent">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-toolbar">
                                        <!--begin::Dropdown-->
                                        <!--end::Dropdown-->
                                        <!--begin::Button-->
                                        <a href="{{ route('Comite.Contact.create', $comite->id) }}" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>Nouveau contact</a>
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Search Form-->
                                    <!--begin::Search Form-->
                                    <div class="mb-7">
                                        <div class="row align-items-center">
                                            <div class="col-lg-9 col-xl-8">
                                                <div class="row align-items-center">
                                                    <div class="col-md-4 my-2 my-md-0">
                                                        <div class="input-icon">
                                                            <input type="text" class="form-control" placeholder="Recherche..." id="kt_datatable_search_query" />
                                                            <span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Search Form-->
                                    <!--end: Search Form-->
                                    <!--begin: Datatable-->
                                    <table class="datatable datatable-bordered datatable-head-custom" id="listeContact">
                                        <thead>
                                        <tr>
                                            <th title="id">id</th>
                                            <th title="Field #2">Identité</th>
                                            <th title="Field #3">Coordonnée</th>
                                            <th title="Field #5">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($comite->contacts as $contact)
                                            <tr>
                                                <td>{{ $contact->id }}</td>
                                                <td>{{ $contact->user->name }}</td>
                                                <td>
                                                    <i class="la la-building"></i>: {{ $contact->position }}<br>
                                                    <i class="la la-mobile"></i>: {{ $contact->telephone }}<br>
                                                    <i class="la la-envelope"></i>: {{ $contact->user->email }}
                                                </td>
                                                <td>
                                                    <a href="tel:{{ $contact->telephone }}" class="btn btn-sm btn-icon btn-skype" data-toggle="tooltip" title="Appeler le contact"><i class="la la-phone-alt"></i> </a>
                                                    <a href="{{ route('Comite.Contact.edit', [$comite->id, $contact->id]) }}" class="btn btn-sm btn-icon btn-info" data-toggle="tooltip" title="Editer le contact"><i class="la la-edit"></i> </a>
                                                    <a href="{{ route('Comite.Contact.delete', [$comite->id, $contact->id]) }}" class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" title="Supprimer le contact"><i class="la la-trash"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment">
                            <div class="card card-custom card-transparent">
                                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                                    <div class="card-toolbar">
                                        <!--begin::Dropdown-->
                                        <!--end::Dropdown-->
                                        <!--begin::Button-->
                                        <a href="{{ route('Comite.Payment.create', $comite->id) }}" class="btn btn-primary font-weight-bolder">
											<span class="svg-icon svg-icon-md">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<circle fill="#000000" cx="9" cy="15" r="6" />
														<path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>Nouveau Mode de Paiement</a>
                                        <!--end::Button-->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <!--begin: Search Form-->
                                    <!--begin::Search Form-->
                                    <div class="mb-7">
                                        <div class="row align-items-center">
                                            <div class="col-lg-9 col-xl-8">
                                                <div class="row align-items-center">
                                                    <div class="col-md-4 my-2 my-md-0">
                                                        <div class="input-icon">
                                                            <input type="text" class="form-control" placeholder="Recherche..." id="kt_datatable_search_query" />
                                                            <span>
																	<i class="flaticon2-search-1 text-muted"></i>
																</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Search Form-->
                                    <!--end: Search Form-->
                                    <!--begin: Datatable-->
                                    <table class="datatable datatable-bordered datatable-head-custom" id="listePayment">
                                        <thead>
                                        <tr>
                                            <th title="id">#</th>
                                            <th title="mode_paiement">Mode de Paiement</th>
                                            <th title="information">Information</th>
                                            <th title="status">Etat</th>
                                            <th title="actions">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($comite->payments as $payment)
                                            <tr>
                                                <td>{{ $payment->id }}</td>
                                                <td>{{ \App\Helpers\ComitePayment::modeAsString(\App\Helpers\Comite::StripeGetPaymentInfo($payment->stripe_id, 'type')) }}</td>
                                                <td>
                                                    @if(\App\Helpers\Comite::StripeGetPaymentInfo($payment->stripe_id, 'type') == 'card')
                                                        {!! \App\Helpers\ComitePayment::cardIconShow(\App\Helpers\Comite::StripeGetCardInfo($payment->stripe_id, 'brand')) !!} XXXX XXXX XXXX {{ \App\Helpers\Comite::StripeGetCardInfo($payment->stripe_id, 'last4') }}<br>
                                                        <strong>Exp:</strong> {{ \App\Helpers\Comite::StripeGetCardInfo($payment->stripe_id, 'exp_month') }}/{{ \App\Helpers\Comite::StripeGetCardInfo($payment->stripe_id, 'exp_year') }}
                                                    @endif
                                                    @if(\App\Helpers\Comite::StripeGetPaymentInfo($payment->stripe_id, 'type') == 'sepa_debit')
                                                        {{ \App\Helpers\Comite::StripeGetSepaDebitInfo($payment->stripe_id, 'country') }}XX {{ \App\Helpers\Comite::StripeGetSepaDebitInfo($payment->stripe_id, 'bank_code') }} {{ \App\Helpers\Comite::StripeGetSepaDebitInfo($payment->stripe_id, 'branch_code') }} XXXX XXXX {{ \App\Helpers\Comite::StripeGetSepaDebitInfo($payment->stripe_id, 'last4') }}
                                                    @endif
                                                </td>
                                                <td>{!! \App\Helpers\ComitePayment::statusPayment($payment->verified) !!}</td>
                                                <td>
                                                    @if($payment->verified == 0)
                                                    <a href="#confirmVerifiedPayment" class="btn btn-sm btn-icon btn-success" data-toggle="modal"><i class="la la-check-circle"></i> </a>
                                                    @endif
                                                    <a href="{{ route('Comite.Payment.delete', [$comite->id, $payment->id]) }}" class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" title="Supprimer le mode de paiement"><i class="la la-trash"></i> </a>
                                                </td>
                                            </tr>
                                            <div class="modal fade" id="confirmVerifiedPayment" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title"><i class="la la-check-circle text-success"></i> Vérification du mode de paiement</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <i aria-hidden="true" class="ki ki-close"></i>
                                                            </button>
                                                        </div>
                                                        <form class="form" id="formConfirmVerifiedPayment" action="{{ route('authorized') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="type" value="confirmVerifiedPayment">
                                                            <input type="hidden" name="intent" value="{{ $payment->id }}">
                                                            <div class="modal-body">
                                                                <div class="alert alert-custom alert-outline-2x alert-outline-primary fade show mb-5" role="alert">
                                                                    <div class="alert-icon"><i class="la la-key"></i></div>
                                                                    <div class="alert-text">La vérification d'un mode de paiement est obligatoire. Si elle n'est pas vérifier, le mode de paiement ne peut être utiliser pour les paiement ultérieur.</div>
                                                                </div>
                                                                @if(auth()->user()->google2fa_secret != null)
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="code" placeholder="code TOTP">
                                                                </div>
                                                                @else
                                                                    <div class="alert alert-custom alert-outline-2x alert-outline-danger fade show mb-5" role="alert">
                                                                        <div class="alert-icon"><i class="la la-times-circle"></i></div>
                                                                        <div class="alert-text">Vous ne disposer pas des codes de vérifications pour effectuer cette fonctions.</div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Authoriser</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!--end: Datatable-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("script")
    <script src="js/comite/show.js"></script>
@endsection
