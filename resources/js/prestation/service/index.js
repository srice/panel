let tableFamille;
let tableService;
let divModule = document.getElementById('divModule');
divModule.style.display = 'none';
let spinnerClass = 'spinner spinner-right spinner-success pr-15';
let spinnerMessage = 'Veuillez patienter';


function listeFamille() {
    tableFamille = $("#listeFamille").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#kt_datatable_search_query_famille'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 50
            },
            {
                field: 'Action',
                autoHide: false,
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}
function listeService() {
    tableService = $("#listeServices").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#kt_datatable_search_query'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 50
            },
            {
                field: 'Action',
                autoHide: false,
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}

function validateFormFamille() {
    let vff = FormValidation.formValidation(document.getElementById('formAddFamille'), {
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: "Le nom de la famille de service est requis"
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            //tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            bootstrap: new FormValidation.plugins.Bootstrap(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            })
        }
    })
        .on('core.form.valid', function (e) {
            let form = $("#formAddFamille");
            let btn = KTUtil.getById('btnSubmitFormFamille');
            let url = form.attr('action');
            let data = form.serializeArray();

            KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                statusCode: {
                    201: function (data) {
                        KTUtil.btnRelease(btn);
                        tableFamille.reload();
                        toastr.success(`La famille de service <strong>${data.name}</strong> à été créer avec succès`, "Succès")
                    },
                    422: function (data) {
                        KTUtil.btnRelease(btn);
                        Array.from(data.data.errors).forEach((err) => {
                            toastr.warning(err, "Validation")
                        })
                    },
                    500: function (jqxhr) {
                        KTUtil.btnRelease(btn);
                        toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                        console.log(jqxhr.responseText)
                    }
                }
            })
        })
}
function validateFormService() {
    let vff = FormValidation.formValidation(document.getElementById('formAddService'), {
        fields: {
            famille_id: {
                validators: {
                    notEmpty: {
                        message: "La famille est requise"
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: "Le nom du service est requis"
                    }
                }
            },
            kernel: {
                validators: {
                    notEmpty: {
                        message: "Le type de service est requis"
                    }
                }
            },
            name_tarif: {
                validators: {
                    notEmpty: {
                        message: "Le nom du tarif de service est requis"
                    }
                }
            },
            montant_tarif: {
                validators: {
                    notEmpty: {
                        message: "Le montant du tarif de service est requis"
                    }
                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            //tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            bootstrap: new FormValidation.plugins.Bootstrap(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            })
        }
    })
        .on('core.form.valid', function (e) {
            let form = $("#formAddService");
            let btn = KTUtil.getById('btnSubmitFormService');
            let url = form.attr('action');
            let data = form.serializeArray();

            KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                statusCode: {
                    201: function (data) {
                        KTUtil.btnRelease(btn);
                        tableService.reload();
                        toastr.success(`Le service <strong>${data.name}</strong> à été créer avec succès`, "Succès")
                    },
                    422: function (data) {
                        KTUtil.btnRelease(btn);
                        Array.from(data.data.errors).forEach((err) => {
                            toastr.warning(err, "Validation")
                        })
                    },
                    500: function (jqxhr) {
                        KTUtil.btnRelease(btn);
                        toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                        console.log(jqxhr.responseText)
                    }
                }
            })
        })
}
function deleteFamille() {
    tableFamille.on('click', '#btnDeleteFamille', function (e) {
        e.preventDefault();
        let btn = $(this);
        let url = btn.attr('href');

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: "GET",
            statusCode: {
                201: function (data) {
                    KTUtil.btnRelease(btn);
                    tableFamille.reload();
                    toastr.success(`La famille <strong>${data.name}</strong> à été supprimer avec succès`, "Succès")
                },
                203: function (data) {
                    KTUtil.btnRelease(btn);
                    toastr.warning(`La famille <strong>${data.name}</strong> n'à pas été supprimer car des services lui sont affilié`, "Attention")
                },
                500: function (jqxhr) {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                    console.log(jqxhr.responseText)
                }
            }
        })
    })
}

function deleteService() {
    tableService.on('click', 'btnDeleteService', (e) => {
        e.preventDefault();
        let btn = KTUtil.getById($(this));
        let url = $(this).attr('href');

        KTUtil.btnWait(btn, spinnerClass);

        $.ajax({
            url: url,
            method: 'GET',
            success: (data) => {
                KTUtil.btnRelease(btn)
            },
            error: (jqxhr) => {
                KTUtil.btnRelease(btn)
            }
        })
    })
}

jQuery(document).ready(function() {
    $.noConflict();
    listeService();
    listeFamille();
    validateFormFamille();
    deleteFamille();
    validateFormService();
    deleteService();
    $("#selectKernel").on('change', function () {
        let value = $(this).val();
        if(value == 2) {
            divModule.style.display = 'block';
        }else{
            divModule.style.display = 'none';
        }
    })
});
