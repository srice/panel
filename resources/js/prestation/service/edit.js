let spinnerClass = 'spinner spinner-right spinner-success pr-15';
let spinnerMessage = 'Veuillez patienter';

function submitEditService() {
    $("#formEditService").on('submit', function (e) {
        e.preventDefault();
        let form = $(this);
        let btn = KTUtil.getById('btnSubmitEditForm');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: "POST",
            data: data,
            statusCode: {
                201: (data) => {
                    KTUtil.btnRelease(btn)
                    toastr.success(`Le service <strong>${data.name}</strong> à été éditer avec succès`)
                    setTimeout(function () {
                        window.location.href='/prestation/service/'+data.id
                    }, 1500)
                },
                422: (data) => {
                    KTUtil.btnRelease(btn)
                    toastr.warning(data.data)
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn)
                    toastr.error("Erreur 500: Erreur de script")
                    console.error(jqxhr.responseText)
                }
            }
        })
    })
}

jQuery(document).ready(function () {
    $.noConflict();
    submitEditService();
});
