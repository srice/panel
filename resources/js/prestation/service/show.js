let tableListeTarifs;
let spinnerClass = 'spinner spinner-right spinner-success pr-15';
let spinnerMessage = 'Veuillez patienter';

function queryListeTable() {
    tableListeTarifs = $("#listeTarifs").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_table_tarifs'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 10,
                autoHide: true
            },
            {
                field: 'name',
                width: 50
            },
            {
                field: 'amount',
                width: 50
            },
            {
                field: 'Action',
                autoHide: false
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}

function formSubmitAddTarif() {
    $("#formAddTarif").on('submit', function (e) {
        e.preventDefault();
        let form = $(this);
        let btn = KTUtil.getById('btnSubmitForm');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            statusCode: {
                201: () => {
                    KTUtil.btnRelease(btn);
                    toastr.success("Le tarif à été ajouté avec succès");
                    $("#addTarif").modal('hide');
                    tableListeTarifs.reload();
                },
                422: (data) => {
                    KTUtil.btnRelease(btn);
                    toastr.warning(data.data)
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur 500: Erreur de script");
                    console.error(jqxhr.responseText)
                }
            }
        })
    })
}

jQuery(document).ready(function () {
    $.noConflict();
    queryListeTable();
    formSubmitAddTarif();
});
