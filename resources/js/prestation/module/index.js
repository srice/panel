let spinnerClass = 'spinner spinner-right spinner-success pr-15';
let spinnerMessage = 'Veuillez patienter';

function submitFormAddModule() {
    $("#formAddModule").on('submit', (e) => {
        e.preventDefault();
        let form = $("#formAddModule");
        let btn = KTUtil.getById('btnSubmitAddModule');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: "POST",
            data: data,
            statusCode: {
                201: (data) => {
                    KTUtil.btnRelease(btn);
                    toastr.success(`Le module <strong>${data.name}</strong> à été ajouté`);
                    setTimeout(() => {
                        window.location.reload()
                    }, 1500)
                },
                422: () => {
                    KTUtil.btnRelease(btn);
                    toastr.warning("Erreur de Validation")
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    console.log(jqxhr.responseText);
                    toastr.error("Erreur 500: Erreur de script")
                }
            }
        })
    });
}

jQuery(document).ready(function() {
    submitFormAddModule()
});
