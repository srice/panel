import datepicker from 'bootstrap-datepicker/dist/js/bootstrap-datepicker'

let spinnerClass = 'spinner spinner-right spinner-success pr-15';
let spinnerMessage = 'Veuillez patienter';

let KTQuilDemos = function() {

    // Private functions
    let demo1 = function() {
        let quill = new Quill('#kt_quil_1', {
            modules: {
                toolbar: [
                    [{
                        header: [1, 2, false]
                    }],
                    ['bold', 'italic', 'underline'],
                    ['image', 'code-block']
                ]
            },
            placeholder: 'Type your text here...',
            theme: 'snow' // or 'bubble'
        });
    };

    let demo2 = function() {
        let quill = new Quill('#quil_task_description', {
            modules: {
                toolbar: [
                    [{
                        header: [1, 2, false]
                    }],
                    ['bold', 'italic', 'underline'],
                    ['image', 'code-block']
                ]
            },
            placeholder: 'Type your text here...',
            theme: 'snow' // or 'bubble'
        });
    };

    return {
        // public functions
        init: function() {
            demo1();
            demo2();
        }
    };
}();
let KTBootstrapDatepicker = function () {

    let arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    // Private functions
    let demos = function () {
        // minimum setup
        $('#kt_datepicker_1').datepicker({
            rtl: KTUtil.isRTL(),
            todayHighlight: true,
            orientation: "bottom left",
            templates: arrows
        });
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

function submitFormEditInfo() {
    $("#formEditModule").on('submit', (e) => {
        e.preventDefault();
        let form = $("#formEditModule");
        let btn = KTUtil.getById('btnEditFormModule');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: "PUT",
            data: data,
            statusCode: {
                201: () => {
                    KTUtil.btnRelease(btn);
                    toastr.success("Les informations du module ont été mise à jour")
                },
                422: () => {
                    KTUtil.btnRelease(btn);
                    toastr.warning("Erreur de Validation")
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur 500: Erreur de script");
                    console.error(jqxhr.responseText)
                }
            }
        })
    })
}
function submitFormEditDesc() {
    $("#formEditDescModule").on('submit', (e) => {
        let field = document.querySelector('#kt_quil_1').children[0].innerHTML;
        e.preventDefault();
        let form = $("#formEditDescModule");
        let btn = KTUtil.getById('btnEditDescFormModule');
        let url = form.attr('action');
        let data = {'description': field};

        KTUtil.btnWait(btn, spinnerClass, spinnerMessage);

        $.ajax({
            url: url,
            method: "POST",
            data: data,
            statusCode: {
                201: () => {
                    KTUtil.btnRelease(btn);
                    toastr.success("La description du module à été mise à jour")
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur 500: Erreur de script");
                    console.error(jqxhr.responseText)
                }
            }
        })
    })
}

jQuery(document).ready(function() {
    new KTImageInput('kt_image_1');
    KTQuilDemos.init();
    KTBootstrapDatepicker.init();

    submitFormEditInfo();
    submitFormEditDesc();

});
