let table;

function listeComite() {
    table = $("#listeComite").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#kt_datatable_search_query'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 50
            },
            {
                field: 'Action',
                autoHide: false,
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}

jQuery(document).ready(function() {
    $.noConflict();
    listeComite()
});
