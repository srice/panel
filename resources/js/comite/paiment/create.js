require('../../core/spinner');

function validateForm()
{
    let fv = FormValidation.formValidation(document.getElementById('formAddPaiment'), {
        fields: {
            iban: {
                validators: {
                    iban: {
                        message: "Cette IBAN est invalide"
                    }
                }
            },
            bic: {
                validators: {
                    bic: {
                        message: "Ce numéro BIC est invalide"
                    }
                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            //tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            bootstrap: new FormValidation.plugins.Bootstrap(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            })
        }
    }).on('core.form.valid', function (e) {
        let form = $("#formAddPaiment");
        let btn = KTUtil.getById('btnSubmitForm');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "POST",
            data: data,
            statusCode: {
                201: function (data) {
                    KTUtil.btnRelease(btn);
                    toastr.success(`Le mode de paiement <strong>${data.iban}</strong> à été ajouter avec succès`, "Succès")
                },
                422: function (data) {
                    KTUtil.btnRelease(btn);
                    Array.from(data.data.errors).forEach((err) => {
                        toastr.warning(err, "Validation")
                    })
                },
                500: function (jqxhr) {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                    console.log(jqxhr.responseText)
                }
            }
        })
    })
}

validateForm();
