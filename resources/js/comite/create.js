const TEST_CARD_NUMBERS = ['4242424242424242'];
const VALID_CARD_NUMBER = '4444111144441111';

require('../core/spinner');

function showFormNone() {
    $("#form_cb").css('display', 'none');
    $("#form_prlv").css('display', 'none')
}

function formWidget() {
    $("input[name='codePostal']").inputmask('99999');
    $("input[name='tel']").inputmask('9999999999');
    $("input[name='telephone']").inputmask('9999999999');
    $("input[name='cbnumber']").inputmask('9999 9999 9999 9999');
    $("input[name='exp_month']").inputmask('99');
    $("input[name='exp_year']").inputmask('99');
    $("input[name='cvv']").inputmask('999');
    $("input[name='iban']").inputmask('AA99 9999 9999 9999 9999 9999 999')
}

function validateForm() {
    let fv = FormValidation.formValidation(document.getElementById('formAddComite'), {
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Le nom du comité est requis'
                    },
                }
            },
            adresse: {
                validators: {
                    notEmpty: {
                        message: 'L\'adresse postal du comité est requis'
                    },
                }
            },
            codePostal: {
                validators: {
                    notEmpty: {
                        message: 'Le code postal du comité est requis'
                    },
                    stringLength: {
                        min:5,
                        max:5,
                        message: 'Un code postal français à 5 chiffres'
                    }
                }
            },
            ville: {
                validators: {
                    notEmpty: {
                        message: 'La ville du comité est requis'
                    }
                }
            },
            tel: {
                validators: {
                    notEmpty: {
                        message: 'Le numéro de téléphone du comité est requis'
                    },
                    phone: {
                        country: "FR",
                        message: "La valeur entrée n'est pas un numéro de téléphone français valide"
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'L\'adresse mmail du comité est requis'
                    },
                    emailAddress: {
                        message: 'La valeur entrée n\'est pas une adresse mail valide'
                    }
                }
            },
            position: {
                validators: {
                    notEmpty: {
                        message: 'La position du contact au sein du comité est requise'
                    }
                }
            },
            telephone: {
                validators: {
                    notEmpty: {
                        message: 'Le numéro de téléphone du contact est requis'
                    },
                    phone: {
                        country: "FR",
                        message: "La valeur entrée n'est pas un numéro de téléphone français valide"
                    }
                }
            },
            bic: {
                validators: {
                    bic: {
                        message: "Ce numéro BIC est invalide"
                    }
                }
            },
            iban: {
                validators: {
                    iban: {
                        message: "Cette IBAN est invalide"
                    }
                }
            },
            cbnumber: {
                validators: {
                    creditCard: {
                        message: "Ce numéro de carte bancaire est invalide"
                    }
                }
            },
            exp_month: {
                validators: {
                    digits: {
                        message: "Le mois d'expiration doit etre uniquement digital"
                    },
                    callback: {
                        message: "Carte Expirée",
                        callback: function (input) {
                            const value = parseInt(input.value, 10);
                            const year = $("input[name='exp_year']").val();
                            const currentMonth = new Date().getMonth() + 1;
                            const currentYear = new Date().getFullYear();

                            if(value < 0 || value > 12) {
                                return false;
                            }

                            if(year === '') {
                                return true;
                            }

                            const expYear = parseInt(year, 10);
                            if(expYear > currentYear || (expYear === currentYear && value >= currentMonth)) {
                                fv.updateFieldStatus('exp_year', "Valide");
                                return true
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            exp_year: {
                validators: {
                    digits: {
                        message: "L\'année d'expiration doit etre uniquement digital"
                    },
                    callback: {
                        message: "Carte Expirée",
                        callback: function (input) {
                            const value = parseInt(input.value, 10);
                            const year = $("input[name='exp_month']").val();
                            const currentMonth = new Date().getMonth() + 1;
                            const currentYear = new Date().getFullYear();

                            if(value < currentYear || value > currentYear + 10) {
                                return false;
                            }

                            if(month == '') {
                                return true;
                            }

                            const expMonth = parseInt(month, 10);
                            if(expYear > currentYear || (expYear === currentYear && expMonth >= currentMonth)) {
                                fv.updateFieldStatus('exp_month', "Valide");
                                return true
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            cvv: {
                validators: {
                    stringLength: {
                        min:3,
                        max:3,
                        message: 'Le code CVV est invalide'
                    }
                }
            },
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            //tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            bootstrap: new FormValidation.plugins.Bootstrap(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            })
        }
    })
        .on('core.form.valid', function (e) {
            //e.preventDefault()
            let form = $("#formAddComite");
            let btn = KTUtil.getById('btnSubmitForm');
            let url = form.attr('action');
            let data = form.serializeArray();

            KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                statusCode: {
                    201: function (data) {
                        KTUtil.btnRelease(btn);
                        toastr.success(`Le comité <strong>${data.name}</strong> à été créer avec succès`, "Succès")
                    },
                    422: function (data) {
                        KTUtil.btnRelease(btn);
                        Array.from(data.data.errors).forEach((err) => {
                            toastr.warning(err, "Validation")
                        })
                    },
                    500: function (jqxhr) {
                        KTUtil.btnRelease(btn);
                        toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                        console.log(jqxhr.responseText)
                    }
                }
            })

        })
}

jQuery(document).ready(function() {
    $.noConflict();
    //formWidget()
    showFormNone();
    $("input[name$='methode']").click(function () {
        let value = $(this).val();
        if(value == 0) {
            $("#form_cb").css('display', 'none');
            $("#form_prlv").css('display', 'none')
        } else if(value == 1) {
            $("#form_cb").css('display', 'block');
            $("#form_prlv").css('display', 'none')
        } else if(value == 2) {
            $("#form_cb").css('display', 'none');
            $("#form_prlv").css('display', 'block')
        }
    });
    validateForm()
});
