require('../../core/spinner');

function submitContactForm() {
    let form = $("#formAddContact");

    form.on('submit', (e) => {
        e.preventDefault();
        let btn = KTUtil.getById('btnSubmitForm');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "POST",
            data: data,
            statusCode: {
                201: (data) => {
                    KTUtil.btnRelease(btn);
                    toastr.success("Le contact <strong>"+data.data.user.name+"</strong> à été ajouter avec succès");
                },
                422: (data) => {
                    KTUtil.btnRelease(btn);
                    Array.from(data.data.errors).forEach((err) => {
                        toastr.warning(err, "Validation")
                    })
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                    console.log(jqxhr.responseText)
                }
            }
        })
    })
}

submitContactForm();
