let listePayment;
require('../core/spinner');

function updateComite() {
    let form = $("#formEditComite");
    let btn = KTUtil.getById('btnUpdateForm');
    let url = form.attr('action');
    let data = form.serializeArray();

    form.on('submit', function (e) {
        e.preventDefault();
        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "PUT",
            data: data,
            statusCode: {
                201: () => {
                    KTUtil.btnRelease(btn);
                    toastr.success("Le comité à été mise à jour")
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Erreur lors de l'execution du script", "Erreur Système 500");
                    console.log(jqxhr.responseText)
                }
            }
        })
    })
}

function reminderDefaultPayment() {
    let btn = KTUtil.getById('btnRemindDefaultPayment');

    btn.on('click', function (e) {
        e.preventDefault();
        let btn = $(this);
        let url = btn.attr('href');

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            statusCode: {
                200: function () {
                    KTUtil.btnRelease(btn);
                    toastr.success("Une notification vient d'être envoyer au comité");
                }
            }
        })
    })
}

function listeContact() {
    table = $("#listeContact").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#kt_datatable_search_query'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 50
            },
            {
                field: 'Coordonnée',
                autoHide: true
            },
            {
                field: 'Action',
                autoHide: false,
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}

function listPayment() {
    listePayment = $("#listePayment").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#kt_datatable_search_query'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: 'id',
                type: 'number',
                width: 50
            },
            {
                field: 'Action',
                autoHide: false,
            }
        ],

        translate: {
            records: {
                processing: 'Chargement...',
                noRecords: 'Aucun élément correspondant trouvé',
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'Premier',
                            prev: 'Précédent',
                            next: 'Suivant',
                            last: 'Dernier',
                            more: 'Voir plus',
                            input: 'Numéro de Page',
                            select: 'Nombre d\'affichage par page',
                        },
                        info: 'Affichage de l\'élément {{start}} à {{end}} sur {{total}} éléments',
                    },
                },
            },
        },
    })
}

function confirmVerifPayment() {
    let form = $("#formConfirmVerifiedPayment");

    form.on('submit', function (e) {
        e.preventDefault();
        let btn = form.find('button');
        let url = form.attr('action');
        let data = form.serializeArray();

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "post",
            data: data,
            statusCode: {
                201: () => {
                    KTUtil.btnRelease(btn);
                    toastr.success("Le mode de paiement à été vérifier avec succès");
                    $("#confirmVerifiedPayment").modal('hide');
                    listePayment.reload()
                },
                202: () => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Le code TOTP n'est pas valide", "TOTP CODE Erreur")
                },
                420: (data) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Une erreur à eu lieu lors de l'utilisation du code TOTP", "TOTP Erreur 420");
                    console.log(data)
                },
                500: (jqxhr) => {
                    KTUtil.btnRelease(btn);
                    toastr.error("Une erreur à eu lieu lors de l'utilisation du code TOTP", "TOTP Erreur 500");
                    console.log(jqxhr)
                }
            }
        })
    })
}

jQuery(document).ready(function () {
    $.noConflict();
    updateComite();
    reminderDefaultPayment();
    listeContact();
    listPayment();
    confirmVerifPayment()
});
