import * as $ from 'jquery'

let errors = $("#errors");
errors.css('display', 'none')

let spinnerClass = 'spinner spinner-right spinner-white pr-15';
let spinnerMessage = 'Veuillez patienter';

function submitLogin() {
    let form = $("#formLogin")
    form.on('submit', function (e) {
        e.preventDefault()
        let btn = KTUtil.getById('kt_login_signin_submit');
        let url = form.attr('action')
        let data = form.serializeArray()

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: 'post',
            data: data,
            statusCode: {
                200: function () {
                    KTUtil.btnRelease(btn);
                    window.location.href = '/'
                },
                422: function () {
                    KTUtil.btnRelease(btn);
                    errors.css('display', 'block')
                    errors.find('.alert-text').html("Certain champs requis ne sont pas remplie.")
                },
                500: function (jqxhr) {
                    KTUtil.btnRelease(btn);
                    errors.css('display', 'block')
                    errors.find('.alert').removeClass('alert-light-warning').addClass('alert-light-danger')
                    errors.find('.alert-icon').html('<i class="flaticon-time"></i>')
                    errors.find('.alert-text').html(jqxhr.responseJSON.message)
                    console.log(jqxhr.responseJSON.message)
                }
            }
        })
    })
}

function submitPasswordEmail() {
    let form = $("#formPasswordEmail")
    form.on('submit', function (e) {
        e.preventDefault()
        let btn = KTUtil.getById('btn_password_submit_email')
        let url = form.attr('action')
        let data = form.serializeArray()

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "post",
            data: data,
            statusCode: {
                200: function (data) {
                    KTUtil.btnRelease(btn)
                    toastr.success("Un email de confirmation vient de vous êtes adressez.")
                },
                422: function () {
                    KTUtil.btnRelease(btn)
                    errors.css('display', 'block')
                    errors.find('.alert-text').html("Certain champs requis ne sont pas remplie.")
                },
                500: function (jqxhr) {
                    KTUtil.btnRelease(btn)
                    errors.css('display', 'block')
                    errors.find('.alert').removeClass('alert-light-warning').addClass('alert-light-danger')
                    errors.find('.alert-icon').html('<i class="flaticon-time"></i>')
                    errors.find('.alert-text').html(jqxhr.responseJSON.message)
                    console.log(jqxhr.responseJSON.message)
                }
            }
        })
    })
}

function submitPasswordUpdate() {
    let form = $("#formPasswordUpdate")
    form.on('submit', function (e) {
        e.preventDefault()
        let btn = KTUtil.getById('btn_password_submit_update')
        let url = form.attr('action')
        let data = form.serializeArray()

        KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter');

        $.ajax({
            url: url,
            method: "post",
            data: data,
            statusCode: {
                200: function (data) {
                    KTUtil.btnRelease(btn)
                    window.location.href='/?alert=Votre mot de passe à été réinitisalisé'
                },
                422: function () {
                    KTUtil.btnRelease(btn)
                    errors.css('display', 'block')
                    errors.find('.alert-text').html("Certain champs requis ne sont pas remplie.")
                },
                500: function (jqxhr) {
                    KTUtil.btnRelease(btn)
                    errors.css('display', 'block')
                    errors.find('.alert').removeClass('alert-light-warning').addClass('alert-light-danger')
                    errors.find('.alert-icon').html('<i class="flaticon-time"></i>')
                    errors.find('.alert-text').html(jqxhr.responseJSON.message)
                    console.log(jqxhr.responseJSON.message)
                }
            }
        })
    })
}

submitLogin()
submitPasswordEmail()
submitPasswordUpdate()
